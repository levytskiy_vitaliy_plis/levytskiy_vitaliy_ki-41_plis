<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="vl_a" />
        <signal name="vl_d" />
        <signal name="vl_e" />
        <signal name="vl_lol" />
        <signal name="XLXN_13" />
        <signal name="vl_b" />
        <signal name="vl_c" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="vl_res" />
        <port polarity="Input" name="vl_a" />
        <port polarity="Input" name="vl_d" />
        <port polarity="Input" name="vl_e" />
        <port polarity="Input" name="vl_lol" />
        <port polarity="Input" name="vl_b" />
        <port polarity="Input" name="vl_c" />
        <port polarity="Output" name="vl_res" />
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="inv" name="XLXI_6">
            <blockpin signalname="vl_b" name="I" />
            <blockpin signalname="XLXN_13" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_1">
            <blockpin signalname="vl_d" name="I0" />
            <blockpin signalname="vl_a" name="I1" />
            <blockpin signalname="XLXN_17" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_2">
            <blockpin signalname="vl_c" name="I0" />
            <blockpin signalname="XLXN_13" name="I1" />
            <blockpin signalname="XLXN_18" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="XLXN_18" name="I0" />
            <blockpin signalname="XLXN_17" name="I1" />
            <blockpin signalname="XLXN_20" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_4">
            <blockpin signalname="XLXN_19" name="I0" />
            <blockpin signalname="XLXN_20" name="I1" />
            <blockpin signalname="vl_res" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_5">
            <blockpin signalname="vl_e" name="I0" />
            <blockpin signalname="vl_lol" name="I1" />
            <blockpin signalname="XLXN_19" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1648" y="784" name="XLXI_1" orien="R0" />
        <instance x="2288" y="896" name="XLXI_3" orien="R0" />
        <instance x="2944" y="960" name="XLXI_4" orien="R0" />
        <instance x="1360" y="1328" name="XLXI_5" orien="R0" />
        <branch name="vl_a">
            <wire x2="1648" y1="656" y2="656" x1="1136" />
        </branch>
        <branch name="vl_d">
            <wire x2="1648" y1="720" y2="720" x1="1136" />
        </branch>
        <branch name="vl_e">
            <wire x2="1360" y1="1264" y2="1264" x1="752" />
        </branch>
        <instance x="1648" y="1072" name="XLXI_2" orien="R0" />
        <instance x="1328" y="960" name="XLXI_6" orien="R0" />
        <branch name="vl_lol">
            <wire x2="1360" y1="1200" y2="1200" x1="752" />
        </branch>
        <branch name="XLXN_13">
            <wire x2="1600" y1="928" y2="928" x1="1552" />
            <wire x2="1600" y1="928" y2="944" x1="1600" />
            <wire x2="1648" y1="944" y2="944" x1="1600" />
        </branch>
        <branch name="vl_b">
            <wire x2="1328" y1="928" y2="928" x1="768" />
        </branch>
        <branch name="vl_c">
            <wire x2="1648" y1="1008" y2="1008" x1="784" />
        </branch>
        <branch name="XLXN_17">
            <wire x2="2096" y1="688" y2="688" x1="1904" />
            <wire x2="2096" y1="688" y2="768" x1="2096" />
            <wire x2="2288" y1="768" y2="768" x1="2096" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="2096" y1="976" y2="976" x1="1904" />
            <wire x2="2096" y1="832" y2="976" x1="2096" />
            <wire x2="2288" y1="832" y2="832" x1="2096" />
        </branch>
        <branch name="XLXN_19">
            <wire x2="2272" y1="1232" y2="1232" x1="1616" />
            <wire x2="2272" y1="896" y2="1232" x1="2272" />
            <wire x2="2944" y1="896" y2="896" x1="2272" />
        </branch>
        <branch name="XLXN_20">
            <wire x2="2736" y1="800" y2="800" x1="2544" />
            <wire x2="2736" y1="800" y2="832" x1="2736" />
            <wire x2="2944" y1="832" y2="832" x1="2736" />
        </branch>
        <iomarker fontsize="28" x="1136" y="656" name="vl_a" orien="R180" />
        <iomarker fontsize="28" x="1136" y="720" name="vl_d" orien="R180" />
        <iomarker fontsize="28" x="768" y="928" name="vl_b" orien="R180" />
        <iomarker fontsize="28" x="784" y="1008" name="vl_c" orien="R180" />
        <iomarker fontsize="28" x="752" y="1200" name="vl_lol" orien="R180" />
        <iomarker fontsize="28" x="752" y="1264" name="vl_e" orien="R180" />
        <branch name="vl_res">
            <wire x2="3216" y1="864" y2="864" x1="3200" />
            <wire x2="3312" y1="864" y2="864" x1="3216" />
        </branch>
        <iomarker fontsize="28" x="3312" y="864" name="vl_res" orien="R0" />
    </sheet>
</drawing>