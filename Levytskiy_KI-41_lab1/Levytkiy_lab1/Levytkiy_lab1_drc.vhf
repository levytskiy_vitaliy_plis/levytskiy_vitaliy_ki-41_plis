--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : Levytkiy_lab1_drc.vhf
-- /___/   /\     Timestamp : 10/28/2016 14:52:11
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: C:\Xilinx\14.7\ISE_DS\ISE\bin\nt64\unwrapped\sch2hdl.exe -intstyle ise -family virtex4 -flat -suppress -vhdl Levytkiy_lab1_drc.vhf -w D:/lab1/Levytkiy_lab1/Levytkiy_lab1.sch
--Design Name: Levytkiy_lab1
--Device: virtex4
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Levytkiy_lab1 is
   port ( vl_a   : in    std_logic; 
          vl_b   : in    std_logic; 
          vl_c   : in    std_logic; 
          vl_d   : in    std_logic; 
          vl_e   : in    std_logic; 
          vl__d  : in    std_logic; 
          vl_res : out   std_logic);
end Levytkiy_lab1;

architecture BEHAVIORAL of Levytkiy_lab1 is
   attribute BOX_TYPE   : string ;
   signal XLXN_13 : std_logic;
   signal XLXN_17 : std_logic;
   signal XLXN_18 : std_logic;
   signal XLXN_19 : std_logic;
   signal XLXN_20 : std_logic;
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
begin
   XLXI_1 : XOR2
      port map (I0=>vl_d,
                I1=>vl_a,
                O=>XLXN_17);
   
   XLXI_2 : XOR2
      port map (I0=>vl_c,
                I1=>XLXN_13,
                O=>XLXN_18);
   
   XLXI_3 : AND2
      port map (I0=>XLXN_18,
                I1=>XLXN_17,
                O=>XLXN_20);
   
   XLXI_4 : OR2
      port map (I0=>XLXN_19,
                I1=>XLXN_20,
                O=>vl_res);
   
   XLXI_5 : OR2
      port map (I0=>vl_e,
                I1=>vl__d,
                O=>XLXN_19);
   
   XLXI_6 : INV
      port map (I=>vl_b,
                O=>XLXN_13);
   
end BEHAVIORAL;


