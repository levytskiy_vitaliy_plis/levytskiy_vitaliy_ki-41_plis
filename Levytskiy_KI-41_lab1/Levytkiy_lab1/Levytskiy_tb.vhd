-- Vhdl test bench created from schematic D:\lab1\Levytkiy_lab1\Levytkiy_lab1.sch - Fri Oct 28 15:03:14 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY Levytkiy_lab1_Levytkiy_lab1_sch_tb IS
END Levytkiy_lab1_Levytkiy_lab1_sch_tb;
ARCHITECTURE behavioral OF Levytkiy_lab1_Levytkiy_lab1_sch_tb IS 

   COMPONENT Levytkiy_lab1
   PORT( vl_a	:	IN	STD_LOGIC; 
          vl_d	:	IN	STD_LOGIC; 
          vl_e	:	IN	STD_LOGIC; 
          vl_lol : IN STD_LOGIC;
          vl_b	:	IN	STD_LOGIC; 
          vl_c	:	IN	STD_LOGIC; 
          vl_res	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL vl_a	:	STD_LOGIC:='0';
   SIGNAL vl_d	:	STD_LOGIC:='0';
   SIGNAL vl_e	:	STD_LOGIC:='0';
   SIGNAL vl_d	:	STD_LOGIC:='0';
   SIGNAL vl_b	:	STD_LOGIC:='0';
   SIGNAL vl_c	:	STD_LOGIC:='0';
	vl_lol => vl_lol STD_LOGIC:='0';
   SIGNAL vl_res	:	STD_LOGIC:='0';

BEGIN

   UUT: Levytkiy_lab1 PORT MAP(
		vl_a => vl_a, 
		vl_d => vl_d, 
		vl_e => vl_e, 
      	vl_lol => vl_lol, 
		vl_b => vl_b, 
		vl_c => vl_c, 
		vl_res => vl_res
   );

vl_a<=not vl_a after 10 ns;
vl_b<=not vl_b after 20 ns;
vl_c<=not vl_c after 40 ns;
 
vl_d<=not vl_d after 80 ns;
vl_lol<=vl_lol after 160 ns;
vl_e<=not vl_e after 320 ns;
-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
