<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1(7:0)" />
        <signal name="XLXN_2(7:0)" />
        <signal name="gvv_A(7:0)" />
        <signal name="gvv_B(7:0)" />
        <signal name="gvv_PROD(15:0)" />
        <signal name="gvv_mPROD(15:0)" />
        <signal name="vl_gvv_IP(15:0)" />
        <signal name="XLXN_8(7:0)" />
        <signal name="XLXN_9(7:0)" />
        <port polarity="Input" name="gvv_A(7:0)" />
        <port polarity="Input" name="gvv_B(7:0)" />
        <port polarity="Output" name="gvv_PROD(15:0)" />
        <port polarity="Output" name="gvv_mPROD(15:0)" />
        <port polarity="Output" name="vl_gvv_IP(15:0)" />
        <blockdef name="GVV_MULT">
            <timestamp>2016-10-15T19:46:57</timestamp>
            <rect width="336" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="400" y="-108" height="24" />
            <line x2="464" y1="-96" y2="-96" x1="400" />
        </blockdef>
        <blockdef name="GVV_mod_multiplier">
            <timestamp>2016-10-15T19:53:48</timestamp>
            <rect width="352" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="416" y="-108" height="24" />
            <line x2="480" y1="-96" y2="-96" x1="416" />
        </blockdef>
        <blockdef name="GVV_IP">
            <timestamp>2016-10-15T19:57:56</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <block symbolname="GVV_MULT" name="XLXI_1">
            <blockpin signalname="gvv_A(7:0)" name="GVV_A(7:0)" />
            <blockpin signalname="gvv_B(7:0)" name="GVV_B(7:0)" />
            <blockpin signalname="gvv_PROD(15:0)" name="GVV_PROD(15:0)" />
        </block>
        <block symbolname="GVV_mod_multiplier" name="XLXI_2">
            <blockpin signalname="gvv_A(7:0)" name="GVV_A(7:0)" />
            <blockpin signalname="gvv_B(7:0)" name="GVV_B(7:0)" />
            <blockpin signalname="gvv_mPROD(15:0)" name="GVV_mPROD(15:0)" />
        </block>
        <block symbolname="GVV_IP" name="XLXI_3">
            <blockpin signalname="gvv_A(7:0)" name="a(7:0)" />
            <blockpin signalname="gvv_B(7:0)" name="b(7:0)" />
            <blockpin signalname="vl_gvv_IP(15:0)" name="p(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1216" y="496" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1200" y="752" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1168" y="992" name="XLXI_3" orien="R0">
        </instance>
        <branch name="gvv_PROD(15:0)">
            <wire x2="1888" y1="400" y2="400" x1="1680" />
        </branch>
        <branch name="gvv_mPROD(15:0)">
            <wire x2="1888" y1="656" y2="656" x1="1680" />
        </branch>
        <branch name="vl_gvv_IP(15:0)">
            <wire x2="1904" y1="1072" y2="1072" x1="1744" />
        </branch>
        <branch name="gvv_B(7:0)">
            <wire x2="944" y1="464" y2="464" x1="896" />
            <wire x2="976" y1="464" y2="464" x1="944" />
            <wire x2="1024" y1="464" y2="464" x1="976" />
            <wire x2="1216" y1="464" y2="464" x1="1024" />
            <wire x2="944" y1="464" y2="720" x1="944" />
            <wire x2="944" y1="720" y2="1136" x1="944" />
            <wire x2="1008" y1="1136" y2="1136" x1="944" />
            <wire x2="1024" y1="1136" y2="1136" x1="1008" />
            <wire x2="1168" y1="1136" y2="1136" x1="1024" />
            <wire x2="1024" y1="720" y2="720" x1="944" />
            <wire x2="1200" y1="720" y2="720" x1="1024" />
        </branch>
        <branch name="gvv_A(7:0)">
            <wire x2="992" y1="400" y2="400" x1="912" />
            <wire x2="1008" y1="400" y2="400" x1="992" />
            <wire x2="1024" y1="400" y2="400" x1="1008" />
            <wire x2="1216" y1="400" y2="400" x1="1024" />
            <wire x2="1008" y1="400" y2="656" x1="1008" />
            <wire x2="1008" y1="656" y2="1072" x1="1008" />
            <wire x2="1168" y1="1072" y2="1072" x1="1008" />
            <wire x2="1024" y1="656" y2="656" x1="1008" />
            <wire x2="1200" y1="656" y2="656" x1="1024" />
        </branch>
        <iomarker fontsize="28" x="912" y="400" name="gvv_A(7:0)" orien="R180" />
        <iomarker fontsize="28" x="1888" y="400" name="gvv_PROD(15:0)" orien="R0" />
        <iomarker fontsize="28" x="896" y="464" name="gvv_B(7:0)" orien="R180" />
        <iomarker fontsize="28" x="1888" y="656" name="gvv_mPROD(15:0)" orien="R0" />
        <iomarker fontsize="28" x="1904" y="1072" name="vl_gvv_IP(15:0)" orien="R0" />
    </sheet>
</drawing>