-- Vhdl test bench created from schematic D:\Levytskiy_lab2\VL_GVV_ALL.sch - Sat Oct 15 23:15:05 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY VL_GVV_ALL_VL_GVV_ALL_sch_tb IS
END VL_GVV_ALL_VL_GVV_ALL_sch_tb;
ARCHITECTURE behavioral OF VL_GVV_ALL_VL_GVV_ALL_sch_tb IS 

   COMPONENT VL_GVV_ALL
   PORT( gvv_A	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          gvv_B	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          gvv_PROD	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          gvv_mPROD	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          vl_gvv_IP	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0));
   END COMPONENT;

   SIGNAL gvv_A	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL gvv_B	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL gvv_PROD	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL gvv_mPROD	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL vl_gvv_IP	:	STD_LOGIC_VECTOR (15 DOWNTO 0);

BEGIN

   UUT: VL_GVV_ALL PORT MAP(
		gvv_A => gvv_A, 
		gvv_B => gvv_B, 
		gvv_PROD => gvv_PROD, 
		gvv_mPROD => gvv_mPROD, 
		vl_gvv_IP => vl_gvv_IP
   );
 


-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
	gvv_A <= "01111101";
gvv_B <= "01111101";
for i in 1 to 20 loop
	wait for 5ns;
gvv_A <= std_logic_vector(unsigned(gvv_A) + (1));
gvv_B <= std_logic_vector(unsigned(gvv_B) + (1));
wait for 5ns;

end loop;
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
