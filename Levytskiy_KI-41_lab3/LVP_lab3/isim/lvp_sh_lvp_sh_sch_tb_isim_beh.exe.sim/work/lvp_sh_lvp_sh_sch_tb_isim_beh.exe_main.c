/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *IEEE_P_2592010699;
char *UNISIM_P_0947159679;
char *IEEE_P_1242562249;
char *VL_P_2533777724;
char *STD_STANDARD;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_00000000004134447467_2073120511_init();
    unisims_ver_m_00000000001108370118_2467748173_init();
    unisims_ver_m_00000000001108370118_1221053840_init();
    unisims_ver_m_00000000001784029001_1621499669_init();
    unisims_ver_m_00000000001784029001_3469659458_init();
    unisims_ver_m_00000000001784029001_2627161160_init();
    unisims_ver_m_00000000001784029001_0728690828_init();
    unisims_ver_m_00000000001784029001_4157631036_init();
    unisims_ver_m_00000000001784029001_1102016586_init();
    unisims_ver_m_00000000001784029001_3473838987_init();
    unisims_ver_m_00000000001784029001_1211087500_init();
    unisims_ver_m_00000000001784029001_0620006912_init();
    unisims_ver_m_00000000001784029001_1491530423_init();
    unisims_ver_m_00000000001784029001_2057561177_init();
    unisims_ver_m_00000000001784029001_3578310343_init();
    unisims_ver_m_00000000001784029001_3837279223_init();
    unisims_ver_m_00000000001784029001_3537292039_init();
    unisims_ver_m_00000000001784029001_0510585572_init();
    unisims_ver_m_00000000001784029001_0339062356_init();
    unisims_ver_m_00000000001784029001_3424510992_init();
    unisims_ver_m_00000000001784029001_0559147920_init();
    unisims_ver_m_00000000001784029001_3190078350_init();
    unisims_ver_m_00000000001784029001_2974312428_init();
    unisims_ver_m_00000000001784029001_2118191546_init();
    unisims_ver_m_00000000001784029001_2200661382_init();
    unisims_ver_m_00000000001773747898_2336946039_init();
    unisims_ver_m_00000000003848737514_1058825862_init();
    unisims_ver_m_00000000000909115699_2771340377_init();
    unisims_ver_m_00000000003317509437_1759035934_init();
    work_m_00000000003073388556_0924340624_init();
    work_m_00000000003206990656_2488873757_init();
    ieee_p_2592010699_init();
    ieee_p_1242562249_init();
    unisim_p_0947159679_init();
    vl_p_2533777724_init();
    work_a_4046975169_3212880686_init();
    work_a_3283064627_3212880686_init();
    work_a_0746053773_3212880686_init();


    xsi_register_tops("work_a_0746053773_3212880686");
    xsi_register_tops("work_m_00000000004134447467_2073120511");

    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);
    UNISIM_P_0947159679 = xsi_get_engine_memory("unisim_p_0947159679");
    IEEE_P_1242562249 = xsi_get_engine_memory("ieee_p_1242562249");
    VL_P_2533777724 = xsi_get_engine_memory("vl_p_2533777724");
    STD_STANDARD = xsi_get_engine_memory("std_standard");

    return xsi_run_simulation(argc, argv);

}
