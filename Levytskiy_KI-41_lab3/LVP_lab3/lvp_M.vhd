----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:15:22 12/21/2016 
-- Design Name: 
-- Module Name:    lvp_M - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lvp_M is
    Port ( LVP_A : in  STD_LOGIC_VECTOR (7 downto 0);
           LVP_out1 : out  STD_LOGIC_VECTOR (39 downto 0));
end lvp_M;

architecture Behavioral of lvp_M is
signal lvp_prod, lvp_p31,lvp_p30,lvp_p29,lvp_p28,lvp_p27,lvp_p26,lvp_p25,lvp_p24,lvp_p23,lvp_p22,lvp_p21,lvp_p20,lvp_p19,lvp_p18,lvp_p17,lvp_p16,lvp_p15,
lvp_p14,lvp_p13,lvp_p12,lvp_p11,lvp_p10,lvp_p9,lvp_p8,lvp_p7,lvp_p6,lvp_p5,lvp_p4,lvp_p3,lvp_p2,lvp_p1,lvp_p0:unsigned (39 downto 0);
begin
lvp_p0  <="00000000000000000000000000000000" & unsigned (lvp_A) & "";
lvp_p1  <="0000000000000000000000000000000" & unsigned (lvp_A) & "0";
lvp_p2  <="000000000000000000000000000000" & unsigned (lvp_A) & "00";
lvp_p5  <="000000000000000000000000000" & unsigned (lvp_A) & "00000";
lvp_p7  <="0000000000000000000000000" & unsigned (lvp_A) & "0000000";
lvp_p8  <="000000000000000000000000" & unsigned (lvp_A) & "00000000";
lvp_p9  <="00000000000000000000000" & unsigned (lvp_A) & "000000000";
lvp_p10 <="0000000000000000000000" & unsigned (lvp_A) & "0000000000";
lvp_p11 <="000000000000000000000" & unsigned (lvp_A) & "00000000000";
lvp_p13 <="0000000000000000000" & unsigned (lvp_A) & "0000000000000";
lvp_p15 <="00000000000000000" & unsigned (lvp_A) & "000000000000000";
lvp_p17 <="000000000000000" & unsigned (lvp_A) & "00000000000000000";
lvp_p18 <="00000000000000" & unsigned (lvp_A) & "000000000000000000";
lvp_p19 <="0000000000000" & unsigned (lvp_A) & "0000000000000000000";
lvp_p22 <="0000000000" & unsigned (lvp_A) & "0000000000000000000000";
lvp_p24 <="00000000" & unsigned (lvp_A) & "000000000000000000000000";
lvp_p25 <="0000000" & unsigned (lvp_A) & "0000000000000000000000000";
lvp_p26 <="000000" & unsigned (lvp_A) & "00000000000000000000000000";
lvp_prod <= (lvp_p0 + lvp_p1 + lvp_p2 + lvp_p5 + lvp_p7 + lvp_p8 + lvp_p9 + lvp_p10 + lvp_p11 + lvp_p13 + lvp_p15 + lvp_p17 + lvp_p18  +lvp_p19 + lvp_p22 + lvp_p24+ lvp_p25+ lvp_p26);
lvp_out1 <= std_logic_vector (lvp_prod);
end Behavioral;

