<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1(7:0)" />
        <signal name="XLXN_2(7:0)" />
        <signal name="LVP_A(7:0)" />
        <signal name="lvp_ip(34:0)" />
        <signal name="lvp_mM(39:0)" />
        <signal name="lvp_M(39:0)" />
        <port polarity="Input" name="LVP_A(7:0)" />
        <port polarity="Output" name="lvp_ip(34:0)" />
        <port polarity="Output" name="lvp_mM(39:0)" />
        <port polarity="Output" name="lvp_M(39:0)" />
        <blockdef name="lvp_ip">
            <timestamp>2016-12-21T20:18:34</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="lvp_M">
            <timestamp>2016-12-21T20:19:57</timestamp>
            <rect width="304" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-44" height="24" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="lvp_mM">
            <timestamp>2016-12-21T20:49:21</timestamp>
            <rect width="304" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-44" height="24" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <block symbolname="lvp_ip" name="XLXI_1">
            <blockpin signalname="LVP_A(7:0)" name="a(7:0)" />
            <blockpin signalname="lvp_ip(34:0)" name="p(34:0)" />
        </block>
        <block symbolname="lvp_M" name="XLXI_2">
            <blockpin signalname="LVP_A(7:0)" name="LVP_A(7:0)" />
            <blockpin signalname="lvp_M(39:0)" name="LVP_out1(39:0)" />
        </block>
        <block symbolname="lvp_mM" name="XLXI_7">
            <blockpin signalname="LVP_A(7:0)" name="LVP_A(7:0)" />
            <blockpin signalname="lvp_mM(39:0)" name="LVP_out2(39:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1328" y="1152" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1360" y="416" name="XLXI_2" orien="R0">
        </instance>
        <branch name="LVP_A(7:0)">
            <wire x2="1152" y1="384" y2="384" x1="976" />
            <wire x2="1360" y1="384" y2="384" x1="1152" />
            <wire x2="1152" y1="384" y2="752" x1="1152" />
            <wire x2="1376" y1="752" y2="752" x1="1152" />
            <wire x2="1152" y1="752" y2="1232" x1="1152" />
            <wire x2="1328" y1="1232" y2="1232" x1="1152" />
        </branch>
        <branch name="lvp_ip(34:0)">
            <wire x2="2080" y1="1232" y2="1232" x1="1904" />
        </branch>
        <branch name="lvp_mM(39:0)">
            <wire x2="2016" y1="752" y2="752" x1="1808" />
        </branch>
        <branch name="lvp_M(39:0)">
            <wire x2="2000" y1="384" y2="384" x1="1792" />
        </branch>
        <iomarker fontsize="28" x="976" y="384" name="LVP_A(7:0)" orien="R180" />
        <iomarker fontsize="28" x="2000" y="384" name="lvp_M(39:0)" orien="R0" />
        <iomarker fontsize="28" x="2016" y="752" name="lvp_mM(39:0)" orien="R0" />
        <iomarker fontsize="28" x="2080" y="1232" name="lvp_ip(34:0)" orien="R0" />
        <instance x="1376" y="784" name="XLXI_7" orien="R0">
        </instance>
    </sheet>
</drawing>