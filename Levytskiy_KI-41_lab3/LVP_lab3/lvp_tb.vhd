-- Vhdl test bench created from schematic C:\PLIS\LVP_lab3\lvp_sh.sch - Wed Dec 21 22:26:16 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY lvp_sh_lvp_sh_sch_tb IS
END lvp_sh_lvp_sh_sch_tb;
ARCHITECTURE behavioral OF lvp_sh_lvp_sh_sch_tb IS 

   COMPONENT lvp_sh
   PORT( LVP_A	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          lvp_ip	:	OUT	STD_LOGIC_VECTOR (34 DOWNTO 0); 
          lvp_mM	:	OUT	STD_LOGIC_VECTOR (39 DOWNTO 0); 
          lvp_M	:	OUT	STD_LOGIC_VECTOR (39 DOWNTO 0));
   END COMPONENT;

   SIGNAL LVP_A	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL lvp_ip	:	STD_LOGIC_VECTOR (34 DOWNTO 0);
   SIGNAL lvp_mM	:	STD_LOGIC_VECTOR (39 DOWNTO 0);
   SIGNAL lvp_M	:	STD_LOGIC_VECTOR (39 DOWNTO 0);
	constant clk_c : time :=10 ns;

BEGIN

   UUT: lvp_sh PORT MAP(
		LVP_A => LVP_A, 
		lvp_ip => lvp_ip, 
		lvp_mM => lvp_mM, 
		lvp_M => lvp_M
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
   lvp_A <="00000001";
	wait for clk_c;
	lvp_A <="00001111";
	wait for clk_c;	
	lvp_A <="11111111";
	wait for clk_c;
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
