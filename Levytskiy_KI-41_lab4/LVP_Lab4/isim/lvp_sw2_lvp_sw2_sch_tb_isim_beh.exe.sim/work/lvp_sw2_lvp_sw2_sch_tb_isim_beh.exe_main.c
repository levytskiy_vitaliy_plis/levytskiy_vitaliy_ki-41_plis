/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *IEEE_P_3499444699;
char *UNISIM_P_0947159679;
char *IEEE_P_3620187407;
char *IEEE_P_2592010699;
char *STD_STANDARD;
char *IEEE_P_1242562249;
char *VL_P_2533777724;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_00000000004134447467_2073120511_init();
    unisims_ver_m_00000000001108370118_0126171863_init();
    unisims_ver_m_00000000001108370118_1185409170_init();
    unisims_ver_m_00000000001108370118_3102277666_init();
    unisims_ver_m_00000000001773747898_0257217429_init();
    unisims_ver_m_00000000001773747898_2454832694_init();
    unisims_ver_m_00000000001773747898_2336946039_init();
    unisims_ver_m_00000000001784029001_1343423062_init();
    unisims_ver_m_00000000001784029001_2199131516_init();
    unisims_ver_m_00000000001508379050_3852734344_init();
    unisims_ver_m_00000000003848737514_1058825862_init();
    unisims_ver_m_00000000000909115699_2771340377_init();
    unisims_ver_m_00000000000129589818_1491383940_init();
    unisims_ver_m_00000000003927721830_1593237687_init();
    unisims_ver_m_00000000003317509437_1759035934_init();
    work_m_00000000003328066507_0759812947_init();
    unisims_ver_m_00000000000236260522_2449448540_init();
    unisims_ver_m_00000000003405408344_3841093685_init();
    work_m_00000000002832889933_0235888400_init();
    unisims_ver_m_00000000002444920515_2091797430_init();
    work_m_00000000000283126530_1844323905_init();
    work_m_00000000002484691218_3228066112_init();
    ieee_p_2592010699_init();
    ieee_p_1242562249_init();
    unisim_p_0947159679_init();
    vl_p_2533777724_init();
    work_a_2377405256_3212880686_init();
    ieee_p_3499444699_init();
    ieee_p_3620187407_init();
    work_a_0236375912_3212880686_init();
    work_a_1306395277_3212880686_init();
    work_a_1075481895_3212880686_init();
    work_a_0652649356_3212880686_init();
    work_a_1817150055_3212880686_init();
    work_a_1912012200_3212880686_init();


    xsi_register_tops("work_a_1912012200_3212880686");
    xsi_register_tops("work_m_00000000004134447467_2073120511");

    IEEE_P_3499444699 = xsi_get_engine_memory("ieee_p_3499444699");
    UNISIM_P_0947159679 = xsi_get_engine_memory("unisim_p_0947159679");
    IEEE_P_3620187407 = xsi_get_engine_memory("ieee_p_3620187407");
    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);
    STD_STANDARD = xsi_get_engine_memory("std_standard");
    IEEE_P_1242562249 = xsi_get_engine_memory("ieee_p_1242562249");
    VL_P_2533777724 = xsi_get_engine_memory("vl_p_2533777724");

    return xsi_run_simulation(argc, argv);

}
