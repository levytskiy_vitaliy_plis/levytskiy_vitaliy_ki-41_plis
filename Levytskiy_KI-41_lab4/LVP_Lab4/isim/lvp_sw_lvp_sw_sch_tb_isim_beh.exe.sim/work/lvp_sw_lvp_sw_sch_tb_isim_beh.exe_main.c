/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *IEEE_P_2592010699;
char *IEEE_P_1242562249;
char *STD_STANDARD;
char *UNISIM_P_0947159679;
char *VL_P_2533777724;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_00000000004134447467_2073120511_init();
    unisims_ver_m_00000000003286176031_2607553651_init();
    work_m_00000000003434020007_1664473530_init();
    unisims_ver_m_00000000002444920515_2091797430_init();
    unisims_ver_m_00000000001773747898_0257217429_init();
    unisims_ver_m_00000000001773747898_2336946039_init();
    unisims_ver_m_00000000001784029001_1343423062_init();
    unisims_ver_m_00000000001508379050_3852734344_init();
    unisims_ver_m_00000000003848737514_1058825862_init();
    unisims_ver_m_00000000000909115699_2771340377_init();
    unisims_ver_m_00000000000129589818_1491383940_init();
    unisims_ver_m_00000000003317509437_1759035934_init();
    work_m_00000000001810576942_0898211772_init();
    unisims_ver_m_00000000002601448656_3367321443_init();
    work_m_00000000004219630390_0206921359_init();
    unisims_ver_m_00000000001915777083_3411452309_init();
    unisims_ver_m_00000000003948601558_0484350389_init();
    unisims_ver_m_00000000004098988994_0302322055_init();
    unisims_ver_m_00000000000924517765_3125220529_init();
    work_m_00000000000733986110_0298240820_init();
    work_m_00000000002003858277_2627865777_init();
    work_m_00000000003186042580_0126976155_init();
    work_m_00000000002616816127_4234246387_init();
    work_m_00000000000540251826_0424754920_init();
    ieee_p_2592010699_init();
    ieee_p_1242562249_init();
    unisim_p_0947159679_init();
    vl_p_2533777724_init();
    work_a_2147427005_3212880686_init();
    work_a_2567348184_3212880686_init();


    xsi_register_tops("work_a_2567348184_3212880686");
    xsi_register_tops("work_m_00000000004134447467_2073120511");

    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);
    IEEE_P_1242562249 = xsi_get_engine_memory("ieee_p_1242562249");
    STD_STANDARD = xsi_get_engine_memory("std_standard");
    UNISIM_P_0947159679 = xsi_get_engine_memory("unisim_p_0947159679");
    VL_P_2533777724 = xsi_get_engine_memory("vl_p_2533777724");

    return xsi_run_simulation(argc, argv);

}
