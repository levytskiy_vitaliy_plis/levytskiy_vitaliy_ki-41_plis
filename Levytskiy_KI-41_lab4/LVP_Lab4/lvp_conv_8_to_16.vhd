----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:24:21 12/10/2016 
-- Design Name: 
-- Module Name:    lvp_conv_8_to_16 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lvp_conv_8_to_16 is
    Port ( s8 : in  STD_LOGIC_VECTOR (7 downto 0);
           s16 : out  STD_LOGIC_VECTOR (15 downto 0));
end lvp_conv_8_to_16;

architecture Behavioral of lvp_conv_8_to_16 is

begin
 s16 <= X"00" & s8;
end Behavioral;

