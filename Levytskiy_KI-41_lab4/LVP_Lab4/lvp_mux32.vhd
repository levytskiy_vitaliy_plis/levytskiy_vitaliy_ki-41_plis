----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:45:34 12/11/2016 
-- Design Name: 
-- Module Name:    lvp_mux32 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lvp_mux32 is
    Port ( s0 : in  STD_LOGIC;
           Data1 : in  STD_LOGIC_VECTOR (31 downto 0);
           Data2 : in  STD_LOGIC_VECTOR (31 downto 0);
           CLK : in  STD_LOGIC;
           Data_res : out  STD_LOGIC_VECTOR (31 downto 0));
end lvp_mux32;

architecture Behavioral of lvp_mux32 is

BEGIN
process(Data1, Data2, s0, CLK)
begin

case s0 is

    when '0' => Data_res <= Data1;
    when '1' => Data_res <= Data2;
    when others => data_res <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
	 end case;
end process;
end Behavioral;

