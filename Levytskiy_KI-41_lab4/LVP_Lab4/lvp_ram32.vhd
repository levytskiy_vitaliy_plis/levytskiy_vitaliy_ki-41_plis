----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:47:43 12/11/2016 
-- Design Name: 
-- Module Name:    lvp_ram32 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lvp_ram32 is
    Port ( WE : in  STD_LOGIC;
           CE : in  STD_LOGIC;
           OE : in  STD_LOGIC;
           A : in  STD_LOGIC_VECTOR (3 downto 0);
           Data_in : in  STD_LOGIC_VECTOR (31 downto 0);
           Data_out : out  STD_LOGIC_VECTOR (31 downto 0);
           CLK : in  STD_LOGIC);
end lvp_ram32;

architecture Behavioral of lvp_ram32 is
type MEM_2K_4 is array (0 to 15) of std_logic_vector(31 downto 0);
signal RAM_MEM:MEM_2K_4;
begin
	process(OE,WE,A,CLK)
begin
		if OE='1' then
		Data_out<=RAM_MEM(conv_integer(A));
		
	else	
		RAM_MEM(conv_integer(A))<=Data_in; 
	
	end if;
	end process;
end Behavioral;

