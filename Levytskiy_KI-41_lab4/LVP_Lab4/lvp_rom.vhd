----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:51:00 12/11/2016 
-- Design Name: 
-- Module Name:    lvp_rom - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;
use ieee.std_logic_arith.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lvp_rom is
    Port ( CLK : in  STD_LOGIC;
           A : in  STD_LOGIC_VECTOR (3 downto 0);
           D : out  STD_LOGIC_VECTOR (19 downto 0));
end lvp_rom;

architecture Behavioral of lvp_rom is

begin
process (A)
		variable A_temp:integer;
		begin
		A_temp:= conv_integer(A(3 downto 0));	
				case A_temp is
					when 0 => D <= x"00010";	   				   				
					when 1 => D <= x"20411";	   				   				
					when 2 => D <= x"84850";     
					when 3 => D <= x"A4C58";     
					when 4 => D <= x"438D0";
					when 5 => D <= x"01CD3";
					when 6 => D <= x"01090";  	  			
					when 7 => D <= x"63491";  	  			
					when 8 => D <= x"01110";  	  			
					when 9 => D <= x"01520";							
					when others => D <= "ZZZZZZZZZZZZZZZZZZZZ";
				end case;			
	end process;
end Behavioral;

