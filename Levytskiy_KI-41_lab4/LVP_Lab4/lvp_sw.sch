<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="c(7:0)" />
        <signal name="CX" />
        <signal name="CLR" />
        <signal name="b(7:0)" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_9(7:0)" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="a(7:0)" />
        <signal name="d(7:0)" />
        <signal name="XLXN_22(7:0)" />
        <signal name="XLXN_23(7:0)" />
        <signal name="XLXN_24(7:0)" />
        <signal name="XLXN_27(15:0)" />
        <signal name="XLXN_28(15:0)" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="XLXN_33" />
        <signal name="XLXN_34" />
        <signal name="XLXN_35" />
        <signal name="XLXN_36" />
        <signal name="XLXN_37" />
        <signal name="XLXN_38" />
        <signal name="XLXN_39" />
        <signal name="XLXN_40" />
        <signal name="XLXN_41" />
        <signal name="XLXN_42" />
        <signal name="CE" />
        <signal name="XLXN_44" />
        <signal name="XLXN_45" />
        <signal name="XLXN_46" />
        <signal name="XLXN_47" />
        <signal name="XLXN_48" />
        <signal name="XLXN_49" />
        <signal name="XLXN_50" />
        <signal name="XLXN_51(15:0)" />
        <signal name="XLXN_52(15:0)" />
        <signal name="XLXN_53" />
        <signal name="XLXN_54(7:0)" />
        <signal name="XLXN_55(15:0)" />
        <signal name="XLXN_56(15:0)" />
        <signal name="XLXN_57" />
        <signal name="XLXN_58" />
        <signal name="XLXN_60" />
        <signal name="XLXN_61" />
        <signal name="XLXN_62" />
        <signal name="XLXN_63" />
        <signal name="XLXN_64(15:0)" />
        <signal name="XLXN_65(15:0)" />
        <signal name="XLXN_66(15:0)" />
        <signal name="XLXN_67" />
        <signal name="XLXN_68" />
        <signal name="XLXN_69" />
        <signal name="XLXN_70(15:0)" />
        <signal name="XLXN_71(15:0)" />
        <signal name="XLXN_72(15:0)" />
        <signal name="res(31:0)" />
        <signal name="XLXN_75(7:0)" />
        <signal name="XLXN_76(7:0)" />
        <port polarity="Input" name="c(7:0)" />
        <port polarity="Input" name="CX" />
        <port polarity="Input" name="CLR" />
        <port polarity="Input" name="b(7:0)" />
        <port polarity="Input" name="a(7:0)" />
        <port polarity="Input" name="d(7:0)" />
        <port polarity="Input" name="CE" />
        <port polarity="Output" name="res(31:0)" />
        <blockdef name="fd8ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="lvp_ip_mul">
            <timestamp>2016-12-10T19:19:8</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="lvp_conv_8_to_16">
            <timestamp>2016-12-10T19:25:13</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="fd16re">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
            <rect width="64" x="0" y="-268" height="24" />
            <rect width="64" x="320" y="-268" height="24" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
        </blockdef>
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="fd16ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="lvp_ip1">
            <timestamp>2016-12-10T19:37:20</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="lvp_ip_16">
            <timestamp>2016-12-10T19:22:29</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <block symbolname="fd8ce" name="XLXI_1">
            <blockpin signalname="CX" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="c(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_22(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_2">
            <blockpin signalname="CX" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="b(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_23(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_3">
            <blockpin signalname="CX" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="d(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_24(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_4">
            <blockpin signalname="CX" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="a(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_76(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="lvp_ip_mul" name="XLXI_5">
            <blockpin signalname="XLXN_22(7:0)" name="a(7:0)" />
            <blockpin signalname="XLXN_23(7:0)" name="b(7:0)" />
            <blockpin signalname="CX" name="clk" />
            <blockpin signalname="XLXN_28(15:0)" name="p(15:0)" />
        </block>
        <block symbolname="lvp_conv_8_to_16" name="XLXI_6">
            <blockpin signalname="XLXN_24(7:0)" name="s8(7:0)" />
            <blockpin signalname="XLXN_27(15:0)" name="s16(15:0)" />
        </block>
        <block symbolname="fd16re" name="XLXI_7">
            <blockpin signalname="CX" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="XLXN_28(15:0)" name="D(15:0)" />
            <blockpin signalname="CLR" name="R" />
            <blockpin signalname="XLXN_52(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16re" name="XLXI_8">
            <blockpin signalname="CX" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="XLXN_27(15:0)" name="D(15:0)" />
            <blockpin signalname="CLR" name="R" />
            <blockpin signalname="XLXN_51(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_11">
            <blockpin signalname="XLXN_52(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_51(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_53" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_55(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_12">
            <blockpin signalname="XLXN_53" name="G" />
        </block>
        <block symbolname="lvp_conv_8_to_16" name="XLXI_13">
            <blockpin signalname="XLXN_54(7:0)" name="s8(7:0)" />
            <blockpin signalname="XLXN_56(15:0)" name="s16(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_14">
            <blockpin signalname="CX" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_55(15:0)" name="D(15:0)" />
            <blockpin signalname="XLXN_64(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_15">
            <blockpin signalname="CX" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_56(15:0)" name="D(15:0)" />
            <blockpin signalname="XLXN_66(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_16">
            <blockpin signalname="CX" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_76(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_75(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_17">
            <blockpin signalname="CX" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_75(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_54(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="lvp_ip1" name="XLXI_18">
            <blockpin signalname="XLXN_64(15:0)" name="a(15:0)" />
            <blockpin signalname="XLXN_64(15:0)" name="b(15:0)" />
            <blockpin signalname="CX" name="clk" />
            <blockpin signalname="XLXN_70(15:0)" name="p(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_20">
            <blockpin signalname="CX" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_66(15:0)" name="D(15:0)" />
            <blockpin signalname="XLXN_72(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_21">
            <blockpin signalname="CX" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_70(15:0)" name="D(15:0)" />
            <blockpin signalname="XLXN_71(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="lvp_ip_16" name="XLXI_22">
            <blockpin signalname="XLXN_71(15:0)" name="a(15:0)" />
            <blockpin signalname="XLXN_72(15:0)" name="b(15:0)" />
            <blockpin signalname="CX" name="clk" />
            <blockpin signalname="res(31:0)" name="p(31:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="800" y="784" name="XLXI_1" orien="R0" />
        <instance x="816" y="1296" name="XLXI_2" orien="R0" />
        <instance x="832" y="1872" name="XLXI_3" orien="R0" />
        <instance x="848" y="2352" name="XLXI_4" orien="R0" />
        <branch name="c(7:0)">
            <wire x2="800" y1="528" y2="528" x1="464" />
        </branch>
        <branch name="CX">
            <wire x2="592" y1="656" y2="656" x1="464" />
            <wire x2="800" y1="656" y2="656" x1="592" />
            <wire x2="592" y1="656" y2="848" x1="592" />
            <wire x2="1504" y1="848" y2="848" x1="592" />
            <wire x2="592" y1="848" y2="1168" x1="592" />
            <wire x2="816" y1="1168" y2="1168" x1="592" />
            <wire x2="592" y1="1168" y2="1376" x1="592" />
            <wire x2="592" y1="1376" y2="1744" x1="592" />
            <wire x2="832" y1="1744" y2="1744" x1="592" />
            <wire x2="592" y1="1744" y2="2224" x1="592" />
            <wire x2="848" y1="2224" y2="2224" x1="592" />
            <wire x2="1488" y1="1376" y2="1376" x1="592" />
            <wire x2="2224" y1="1376" y2="1376" x1="1488" />
            <wire x2="2224" y1="1376" y2="1744" x1="2224" />
            <wire x2="2304" y1="1744" y2="1744" x1="2224" />
            <wire x2="2224" y1="1744" y2="2224" x1="2224" />
            <wire x2="2320" y1="2224" y2="2224" x1="2224" />
            <wire x2="1488" y1="1376" y2="2224" x1="1488" />
            <wire x2="1648" y1="2224" y2="2224" x1="1488" />
            <wire x2="2208" y1="608" y2="816" x1="2208" />
            <wire x2="2208" y1="816" y2="1360" x1="2208" />
            <wire x2="2224" y1="1360" y2="1360" x1="2208" />
            <wire x2="2224" y1="1360" y2="1376" x1="2224" />
            <wire x2="2288" y1="816" y2="816" x1="2208" />
            <wire x2="2736" y1="608" y2="608" x1="2208" />
            <wire x2="2736" y1="608" y2="1536" x1="2736" />
            <wire x2="3456" y1="1536" y2="1536" x1="2736" />
            <wire x2="3984" y1="1536" y2="1536" x1="3456" />
            <wire x2="3456" y1="1536" y2="1680" x1="3456" />
            <wire x2="3456" y1="1680" y2="2224" x1="3456" />
            <wire x2="3520" y1="2224" y2="2224" x1="3456" />
            <wire x2="3456" y1="2224" y2="2448" x1="3456" />
            <wire x2="4064" y1="2448" y2="2448" x1="3456" />
            <wire x2="4624" y1="2448" y2="2448" x1="4064" />
            <wire x2="4672" y1="1680" y2="1680" x1="3456" />
            <wire x2="3520" y1="1280" y2="1280" x1="3456" />
            <wire x2="3456" y1="1280" y2="1536" x1="3456" />
            <wire x2="3984" y1="1312" y2="1536" x1="3984" />
            <wire x2="4016" y1="1312" y2="1312" x1="3984" />
            <wire x2="4160" y1="2224" y2="2224" x1="4064" />
            <wire x2="4064" y1="2224" y2="2448" x1="4064" />
            <wire x2="4624" y1="2240" y2="2448" x1="4624" />
            <wire x2="4768" y1="2240" y2="2240" x1="4624" />
        </branch>
        <branch name="CLR">
            <wire x2="656" y1="752" y2="752" x1="480" />
            <wire x2="800" y1="752" y2="752" x1="656" />
            <wire x2="656" y1="752" y2="1264" x1="656" />
            <wire x2="816" y1="1264" y2="1264" x1="656" />
            <wire x2="656" y1="1264" y2="1424" x1="656" />
            <wire x2="656" y1="1424" y2="1824" x1="656" />
            <wire x2="656" y1="1824" y2="1840" x1="656" />
            <wire x2="832" y1="1840" y2="1840" x1="656" />
            <wire x2="656" y1="1824" y2="2320" x1="656" />
            <wire x2="848" y1="2320" y2="2320" x1="656" />
            <wire x2="1552" y1="1424" y2="1424" x1="656" />
            <wire x2="2288" y1="1424" y2="1424" x1="1552" />
            <wire x2="2288" y1="1424" y2="1840" x1="2288" />
            <wire x2="2304" y1="1840" y2="1840" x1="2288" />
            <wire x2="2288" y1="1840" y2="2320" x1="2288" />
            <wire x2="2320" y1="2320" y2="2320" x1="2288" />
            <wire x2="2432" y1="1424" y2="1424" x1="2288" />
            <wire x2="1552" y1="1424" y2="2320" x1="1552" />
            <wire x2="1648" y1="2320" y2="2320" x1="1552" />
            <wire x2="2288" y1="912" y2="1424" x1="2288" />
            <wire x2="2432" y1="1376" y2="1424" x1="2432" />
            <wire x2="3488" y1="1376" y2="1376" x1="2432" />
            <wire x2="3520" y1="1376" y2="1376" x1="3488" />
            <wire x2="3488" y1="1376" y2="1776" x1="3488" />
            <wire x2="3488" y1="1776" y2="2320" x1="3488" />
            <wire x2="3520" y1="2320" y2="2320" x1="3488" />
            <wire x2="3488" y1="2320" y2="2496" x1="3488" />
            <wire x2="4160" y1="2496" y2="2496" x1="3488" />
            <wire x2="4672" y1="1776" y2="1776" x1="3488" />
            <wire x2="4160" y1="2320" y2="2496" x1="4160" />
        </branch>
        <iomarker fontsize="28" x="464" y="528" name="c(7:0)" orien="R180" />
        <iomarker fontsize="28" x="464" y="656" name="CX" orien="R180" />
        <iomarker fontsize="28" x="480" y="752" name="CLR" orien="R180" />
        <branch name="b(7:0)">
            <wire x2="816" y1="1040" y2="1040" x1="464" />
        </branch>
        <branch name="a(7:0)">
            <wire x2="848" y1="2096" y2="2096" x1="400" />
        </branch>
        <branch name="d(7:0)">
            <wire x2="832" y1="1616" y2="1616" x1="368" />
        </branch>
        <iomarker fontsize="28" x="464" y="1040" name="b(7:0)" orien="R180" />
        <iomarker fontsize="28" x="368" y="1616" name="d(7:0)" orien="R180" />
        <iomarker fontsize="28" x="400" y="2096" name="a(7:0)" orien="R180" />
        <instance x="1504" y="608" name="XLXI_5" orien="R0">
        </instance>
        <branch name="XLXN_22(7:0)">
            <wire x2="1344" y1="528" y2="528" x1="1184" />
            <wire x2="1344" y1="528" y2="688" x1="1344" />
            <wire x2="1504" y1="688" y2="688" x1="1344" />
        </branch>
        <branch name="XLXN_23(7:0)">
            <wire x2="1344" y1="1040" y2="1040" x1="1200" />
            <wire x2="1344" y1="752" y2="1040" x1="1344" />
            <wire x2="1504" y1="752" y2="752" x1="1344" />
        </branch>
        <instance x="1616" y="1648" name="XLXI_6" orien="R0">
        </instance>
        <branch name="XLXN_24(7:0)">
            <wire x2="1616" y1="1616" y2="1616" x1="1216" />
        </branch>
        <instance x="2288" y="944" name="XLXI_7" orien="R0" />
        <instance x="2304" y="1872" name="XLXI_8" orien="R0" />
        <branch name="XLXN_27(15:0)">
            <wire x2="2304" y1="1616" y2="1616" x1="2000" />
        </branch>
        <branch name="XLXN_28(15:0)">
            <wire x2="2288" y1="688" y2="688" x1="2080" />
        </branch>
        <branch name="CE">
            <wire x2="688" y1="592" y2="592" x1="480" />
            <wire x2="800" y1="592" y2="592" x1="688" />
            <wire x2="688" y1="592" y2="1104" x1="688" />
            <wire x2="816" y1="1104" y2="1104" x1="688" />
            <wire x2="688" y1="1104" y2="1312" x1="688" />
            <wire x2="688" y1="1312" y2="1680" x1="688" />
            <wire x2="832" y1="1680" y2="1680" x1="688" />
            <wire x2="688" y1="1680" y2="2160" x1="688" />
            <wire x2="848" y1="2160" y2="2160" x1="688" />
            <wire x2="1328" y1="1312" y2="1312" x1="688" />
            <wire x2="2112" y1="1312" y2="1312" x1="1328" />
            <wire x2="2112" y1="1312" y2="2160" x1="2112" />
            <wire x2="2320" y1="2160" y2="2160" x1="2112" />
            <wire x2="2192" y1="1312" y2="1312" x1="2112" />
            <wire x2="2192" y1="1312" y2="1680" x1="2192" />
            <wire x2="2304" y1="1680" y2="1680" x1="2192" />
            <wire x2="1328" y1="1312" y2="2160" x1="1328" />
            <wire x2="1648" y1="2160" y2="2160" x1="1328" />
            <wire x2="2128" y1="752" y2="752" x1="2112" />
            <wire x2="2288" y1="752" y2="752" x1="2128" />
            <wire x2="2128" y1="752" y2="944" x1="2128" />
            <wire x2="2656" y1="944" y2="944" x1="2128" />
            <wire x2="2656" y1="944" y2="1456" x1="2656" />
            <wire x2="3408" y1="1456" y2="1456" x1="2656" />
            <wire x2="3408" y1="1456" y2="1616" x1="3408" />
            <wire x2="3408" y1="1616" y2="2160" x1="3408" />
            <wire x2="3520" y1="2160" y2="2160" x1="3408" />
            <wire x2="3408" y1="2160" y2="2384" x1="3408" />
            <wire x2="4016" y1="2384" y2="2384" x1="3408" />
            <wire x2="4672" y1="1616" y2="1616" x1="3408" />
            <wire x2="2112" y1="752" y2="1312" x1="2112" />
            <wire x2="3520" y1="1216" y2="1216" x1="3408" />
            <wire x2="3408" y1="1216" y2="1456" x1="3408" />
            <wire x2="4160" y1="2160" y2="2160" x1="4016" />
            <wire x2="4016" y1="2160" y2="2384" x1="4016" />
        </branch>
        <iomarker fontsize="28" x="480" y="592" name="CE" orien="R180" />
        <instance x="2880" y="1408" name="XLXI_11" orien="R0" />
        <branch name="XLXN_51(15:0)">
            <wire x2="2784" y1="1616" y2="1616" x1="2688" />
            <wire x2="2784" y1="1216" y2="1616" x1="2784" />
            <wire x2="2880" y1="1216" y2="1216" x1="2784" />
        </branch>
        <branch name="XLXN_52(15:0)">
            <wire x2="2768" y1="688" y2="688" x1="2672" />
            <wire x2="2768" y1="688" y2="1088" x1="2768" />
            <wire x2="2880" y1="1088" y2="1088" x1="2768" />
        </branch>
        <instance x="2976" y="912" name="XLXI_12" orien="R0" />
        <branch name="XLXN_53">
            <wire x2="2880" y1="768" y2="960" x1="2880" />
            <wire x2="3040" y1="768" y2="768" x1="2880" />
            <wire x2="3040" y1="768" y2="784" x1="3040" />
        </branch>
        <instance x="2928" y="2128" name="XLXI_13" orien="R0">
        </instance>
        <branch name="XLXN_54(7:0)">
            <wire x2="2928" y1="2096" y2="2096" x1="2704" />
        </branch>
        <instance x="3520" y="1408" name="XLXI_14" orien="R0" />
        <branch name="XLXN_55(15:0)">
            <wire x2="3520" y1="1152" y2="1152" x1="3328" />
        </branch>
        <branch name="XLXN_56(15:0)">
            <wire x2="3520" y1="2096" y2="2096" x1="3312" />
        </branch>
        <instance x="3520" y="2352" name="XLXI_15" orien="R0" />
        <instance x="1648" y="2352" name="XLXI_16" orien="R0" />
        <instance x="2320" y="2352" name="XLXI_17" orien="R0" />
        <instance x="4016" y="1072" name="XLXI_18" orien="R0">
        </instance>
        <branch name="XLXN_64(15:0)">
            <wire x2="3968" y1="1152" y2="1152" x1="3904" />
            <wire x2="4016" y1="1152" y2="1152" x1="3968" />
            <wire x2="3968" y1="1152" y2="1216" x1="3968" />
            <wire x2="4016" y1="1216" y2="1216" x1="3968" />
        </branch>
        <instance x="4160" y="2352" name="XLXI_20" orien="R0" />
        <branch name="XLXN_66(15:0)">
            <wire x2="4160" y1="2096" y2="2096" x1="3904" />
        </branch>
        <instance x="4672" y="1808" name="XLXI_21" orien="R0" />
        <branch name="XLXN_70(15:0)">
            <wire x2="4624" y1="1152" y2="1152" x1="4592" />
            <wire x2="4624" y1="1152" y2="1552" x1="4624" />
            <wire x2="4672" y1="1552" y2="1552" x1="4624" />
        </branch>
        <instance x="4768" y="2000" name="XLXI_22" orien="R0">
        </instance>
        <branch name="XLXN_71(15:0)">
            <wire x2="5408" y1="1856" y2="1856" x1="4704" />
            <wire x2="4704" y1="1856" y2="2080" x1="4704" />
            <wire x2="4768" y1="2080" y2="2080" x1="4704" />
            <wire x2="5408" y1="1552" y2="1552" x1="5056" />
            <wire x2="5408" y1="1552" y2="1856" x1="5408" />
        </branch>
        <branch name="XLXN_72(15:0)">
            <wire x2="4656" y1="2096" y2="2096" x1="4544" />
            <wire x2="4656" y1="2096" y2="2144" x1="4656" />
            <wire x2="4768" y1="2144" y2="2144" x1="4656" />
        </branch>
        <branch name="res(31:0)">
            <wire x2="5392" y1="2480" y2="2480" x1="5008" />
            <wire x2="5008" y1="2480" y2="2576" x1="5008" />
            <wire x2="5072" y1="2576" y2="2576" x1="5008" />
            <wire x2="5392" y1="2080" y2="2080" x1="5344" />
            <wire x2="5392" y1="2080" y2="2464" x1="5392" />
            <wire x2="5392" y1="2464" y2="2480" x1="5392" />
        </branch>
        <iomarker fontsize="28" x="5072" y="2576" name="res(31:0)" orien="R0" />
        <branch name="XLXN_75(7:0)">
            <wire x2="2320" y1="2096" y2="2096" x1="2032" />
        </branch>
        <branch name="XLXN_76(7:0)">
            <wire x2="1648" y1="2096" y2="2096" x1="1232" />
        </branch>
    </sheet>
</drawing>