<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1(31:0)" />
        <signal name="XLXN_2(31:0)" />
        <signal name="res1(31:0)" />
        <signal name="XLXN_6(31:0)" />
        <signal name="XLXN_7(31:0)" />
        <signal name="XLXN_8(31:0)" />
        <signal name="XLXN_9(31:0)" />
        <signal name="XLXN_10(31:0)" />
        <signal name="res2(31:0)" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13(31:0)" />
        <signal name="res(31:0)" />
        <signal name="Counter(3:0)" />
        <signal name="a(7:0)" />
        <signal name="b(7:0)" />
        <signal name="c(7:0)" />
        <signal name="d(7:0)" />
        <signal name="clk" />
        <signal name="XLXN_21" />
        <signal name="XLXN_23" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32(31:0)" />
        <signal name="XLXN_33(31:0)" />
        <signal name="XLXN_34(31:0)" />
        <signal name="XLXN_35(31:0)" />
        <signal name="XLXN_36(31:0)" />
        <signal name="XLXN_37(31:0)" />
        <signal name="XLXN_38(31:0)" />
        <signal name="XLXN_39(31:0)" />
        <signal name="XLXN_40(31:0)" />
        <signal name="XLXN_41(31:0)" />
        <signal name="XLXN_42(31:0)" />
        <signal name="CE" />
        <signal name="XLXN_44" />
        <signal name="XLXN_45" />
        <signal name="XLXN_46" />
        <signal name="XLXN_47" />
        <signal name="ZERO_bit" />
        <signal name="XLXN_49" />
        <signal name="Control(19:0)" />
        <signal name="Control(3)" />
        <signal name="Control(2)" />
        <signal name="Control(1)" />
        <signal name="Control(0)" />
        <signal name="Control(5)" />
        <signal name="Control(4)" />
        <signal name="Control(9:6)" />
        <signal name="Control(10)" />
        <signal name="Control(12:11)" />
        <signal name="Control(16:13)" />
        <signal name="Control(17)" />
        <signal name="Control(19:18)" />
        <port polarity="Output" name="res1(31:0)" />
        <port polarity="Output" name="res2(31:0)" />
        <port polarity="Output" name="res(31:0)" />
        <port polarity="Output" name="Counter(3:0)" />
        <port polarity="Input" name="a(7:0)" />
        <port polarity="Input" name="b(7:0)" />
        <port polarity="Input" name="c(7:0)" />
        <port polarity="Input" name="d(7:0)" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="CE" />
        <port polarity="Input" name="ZERO_bit" />
        <port polarity="Output" name="Control(19:0)" />
        <blockdef name="lvp_mux_4_32">
            <timestamp>2016-12-11T16:54:56</timestamp>
            <rect width="304" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-364" height="24" />
            <line x2="432" y1="-352" y2="-352" x1="368" />
        </blockdef>
        <blockdef name="lvp_ram32">
            <timestamp>2016-12-11T16:49:0</timestamp>
            <rect width="320" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="384" y="-364" height="24" />
            <line x2="448" y1="-352" y2="-352" x1="384" />
        </blockdef>
        <blockdef name="lvp_mux32">
            <timestamp>2016-12-11T16:46:22</timestamp>
            <rect width="304" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-236" height="24" />
            <line x2="432" y1="-224" y2="-224" x1="368" />
        </blockdef>
        <blockdef name="lvp_mul32">
            <timestamp>2016-12-10T19:59:34</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="lvp_add32">
            <timestamp>2016-12-10T20:3:40</timestamp>
            <rect width="224" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="112" y2="112" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" x1="0" />
            <line x2="32" y1="176" y2="176" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="256" y1="112" y2="112" style="linewidth:W" x1="288" />
        </blockdef>
        <blockdef name="lvp_register32">
            <timestamp>2016-12-11T16:53:3</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="lvp_conv_8_to_32">
            <timestamp>2016-12-11T16:50:18</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="lvp_Counter">
            <timestamp>2016-12-10T19:55:37</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="208" y2="208" x1="0" />
            <line x2="544" y1="144" y2="144" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="lvp_rom">
            <timestamp>2016-12-11T17:40:18</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <block symbolname="lvp_mux_4_32" name="XLXI_1">
            <blockpin signalname="clk" name="CLK" />
            <blockpin signalname="Control(19:18)" name="s0(1:0)" />
            <blockpin signalname="XLXN_32(31:0)" name="Data1(31:0)" />
            <blockpin signalname="XLXN_36(31:0)" name="Data2(31:0)" />
            <blockpin signalname="XLXN_37(31:0)" name="Data3(31:0)" />
            <blockpin signalname="XLXN_40(31:0)" name="Data4(31:0)" />
            <blockpin signalname="XLXN_1(31:0)" name="Data_res(31:0)" />
        </block>
        <block symbolname="lvp_mux_4_32" name="XLXI_2">
            <blockpin signalname="clk" name="CLK" />
            <blockpin signalname="Control(12:11)" name="s0(1:0)" />
            <blockpin signalname="XLXN_33(31:0)" name="Data1(31:0)" />
            <blockpin signalname="XLXN_35(31:0)" name="Data2(31:0)" />
            <blockpin signalname="XLXN_37(31:0)" name="Data3(31:0)" />
            <blockpin signalname="XLXN_40(31:0)" name="Data4(31:0)" />
            <blockpin signalname="XLXN_2(31:0)" name="Data_res(31:0)" />
        </block>
        <block symbolname="lvp_ram32" name="XLXI_3">
            <blockpin signalname="ZERO_bit" name="WE" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="Control(17)" name="OE" />
            <blockpin signalname="clk" name="CLK" />
            <blockpin signalname="Control(16:13)" name="A(3:0)" />
            <blockpin signalname="XLXN_1(31:0)" name="Data_in(31:0)" />
            <blockpin signalname="res1(31:0)" name="Data_out(31:0)" />
        </block>
        <block symbolname="lvp_ram32" name="XLXI_4">
            <blockpin signalname="ZERO_bit" name="WE" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="Control(10)" name="OE" />
            <blockpin signalname="clk" name="CLK" />
            <blockpin signalname="Control(9:6)" name="A(3:0)" />
            <blockpin signalname="XLXN_2(31:0)" name="Data_in(31:0)" />
            <blockpin signalname="res2(31:0)" name="Data_out(31:0)" />
        </block>
        <block symbolname="lvp_mux32" name="XLXI_5">
            <blockpin signalname="Control(0)" name="s0" />
            <blockpin signalname="clk" name="CLK" />
            <blockpin signalname="res1(31:0)" name="Data1(31:0)" />
            <blockpin signalname="res2(31:0)" name="Data2(31:0)" />
            <blockpin signalname="XLXN_7(31:0)" name="Data_res(31:0)" />
        </block>
        <block symbolname="lvp_mux32" name="XLXI_6">
            <blockpin signalname="Control(1)" name="s0" />
            <blockpin signalname="clk" name="CLK" />
            <blockpin signalname="res1(31:0)" name="Data1(31:0)" />
            <blockpin signalname="res2(31:0)" name="Data2(31:0)" />
            <blockpin signalname="XLXN_8(31:0)" name="Data_res(31:0)" />
        </block>
        <block symbolname="lvp_mux32" name="XLXI_7">
            <blockpin signalname="Control(2)" name="s0" />
            <blockpin signalname="clk" name="CLK" />
            <blockpin signalname="res1(31:0)" name="Data1(31:0)" />
            <blockpin signalname="res2(31:0)" name="Data2(31:0)" />
            <blockpin signalname="XLXN_9(31:0)" name="Data_res(31:0)" />
        </block>
        <block symbolname="lvp_mux32" name="XLXI_8">
            <blockpin signalname="Control(3)" name="s0" />
            <blockpin signalname="clk" name="CLK" />
            <blockpin signalname="res1(31:0)" name="Data1(31:0)" />
            <blockpin signalname="res2(31:0)" name="Data2(31:0)" />
            <blockpin signalname="XLXN_10(31:0)" name="Data_res(31:0)" />
        </block>
        <block symbolname="lvp_mul32" name="XLXI_9">
            <blockpin signalname="XLXN_7(31:0)" name="a(31:0)" />
            <blockpin signalname="XLXN_8(31:0)" name="b(31:0)" />
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="XLXN_37(31:0)" name="p(31:0)" />
        </block>
        <block symbolname="lvp_add32" name="XLXI_10">
            <blockpin signalname="XLXN_9(31:0)" name="a(31:0)" />
            <blockpin signalname="XLXN_10(31:0)" name="b(31:0)" />
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="CE" name="add" />
            <blockpin signalname="CE" name="ce" />
            <blockpin signalname="XLXN_40(31:0)" name="s(31:0)" />
        </block>
        <block symbolname="lvp_register32" name="XLXI_11">
            <blockpin signalname="Control(5)" name="ld" />
            <blockpin signalname="Control(4)" name="clr" />
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="res2(31:0)" name="d(31:0)" />
            <blockpin signalname="res(31:0)" name="q(31:0)" />
        </block>
        <block symbolname="lvp_conv_8_to_32" name="XLXI_12">
            <blockpin signalname="a(7:0)" name="s8(7:0)" />
            <blockpin signalname="XLXN_36(31:0)" name="s32(31:0)" />
        </block>
        <block symbolname="lvp_conv_8_to_32" name="XLXI_13">
            <blockpin signalname="b(7:0)" name="s8(7:0)" />
            <blockpin signalname="XLXN_33(31:0)" name="s32(31:0)" />
        </block>
        <block symbolname="lvp_conv_8_to_32" name="XLXI_14">
            <blockpin signalname="c(7:0)" name="s8(7:0)" />
            <blockpin signalname="XLXN_32(31:0)" name="s32(31:0)" />
        </block>
        <block symbolname="lvp_conv_8_to_32" name="XLXI_15">
            <blockpin signalname="d(7:0)" name="s8(7:0)" />
            <blockpin signalname="XLXN_35(31:0)" name="s32(31:0)" />
        </block>
        <block symbolname="lvp_Counter" name="XLXI_16">
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="Counter(3:0)" name="q(3:0)" />
        </block>
        <block symbolname="lvp_rom" name="XLXI_17">
            <blockpin signalname="clk" name="CLK" />
            <blockpin signalname="Counter(3:0)" name="A(3:0)" />
            <blockpin signalname="Control(19:0)" name="D(19:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="1168" y="1232" name="XLXI_1" orien="R0">
        </instance>
        <instance x="3008" y="1232" name="XLXI_2" orien="R0">
        </instance>
        <instance x="3712" y="1232" name="XLXI_4" orien="R0">
        </instance>
        <instance x="2016" y="1232" name="XLXI_3" orien="R0">
        </instance>
        <instance x="848" y="2400" name="XLXI_5" orien="R0">
        </instance>
        <instance x="848" y="2848" name="XLXI_6" orien="R0">
        </instance>
        <instance x="832" y="3232" name="XLXI_7" orien="R0">
        </instance>
        <instance x="832" y="3616" name="XLXI_8" orien="R0">
        </instance>
        <instance x="1664" y="2336" name="XLXI_9" orien="R0">
        </instance>
        <instance x="1840" y="3024" name="XLXI_10" orien="R0">
        </instance>
        <instance x="496" y="1440" name="XLXI_12" orien="R0">
        </instance>
        <instance x="496" y="1552" name="XLXI_13" orien="R0">
        </instance>
        <instance x="496" y="1696" name="XLXI_14" orien="R0">
        </instance>
        <instance x="496" y="1824" name="XLXI_15" orien="R0">
        </instance>
        <instance x="304" y="656" name="XLXI_16" orien="R0">
        </instance>
        <instance x="432" y="464" name="XLXI_17" orien="R0">
        </instance>
        <branch name="XLXN_1(31:0)">
            <wire x2="1808" y1="880" y2="880" x1="1600" />
            <wire x2="1808" y1="880" y2="1200" x1="1808" />
            <wire x2="2016" y1="1200" y2="1200" x1="1808" />
        </branch>
        <branch name="XLXN_2(31:0)">
            <wire x2="3568" y1="880" y2="880" x1="3440" />
            <wire x2="3568" y1="880" y2="1200" x1="3568" />
            <wire x2="3712" y1="1200" y2="1200" x1="3568" />
        </branch>
        <branch name="res1(31:0)">
            <wire x2="1072" y1="2000" y2="2000" x1="720" />
            <wire x2="720" y1="2000" y2="2304" x1="720" />
            <wire x2="720" y1="2304" y2="2752" x1="720" />
            <wire x2="720" y1="2752" y2="3136" x1="720" />
            <wire x2="720" y1="3136" y2="3520" x1="720" />
            <wire x2="832" y1="3520" y2="3520" x1="720" />
            <wire x2="832" y1="3136" y2="3136" x1="720" />
            <wire x2="848" y1="2752" y2="2752" x1="720" />
            <wire x2="848" y1="2304" y2="2304" x1="720" />
            <wire x2="1072" y1="1808" y2="1808" x1="1056" />
            <wire x2="1072" y1="1808" y2="1824" x1="1072" />
            <wire x2="2640" y1="1824" y2="1824" x1="1072" />
            <wire x2="1056" y1="1808" y2="1984" x1="1056" />
            <wire x2="1072" y1="1984" y2="1984" x1="1056" />
            <wire x2="1072" y1="1984" y2="2000" x1="1072" />
            <wire x2="2640" y1="880" y2="880" x1="2464" />
            <wire x2="2640" y1="880" y2="1488" x1="2640" />
            <wire x2="2640" y1="1488" y2="1648" x1="2640" />
            <wire x2="2640" y1="1648" y2="1824" x1="2640" />
            <wire x2="2848" y1="1648" y2="1648" x1="2640" />
        </branch>
        <branch name="XLXN_7(31:0)">
            <wire x2="1648" y1="2176" y2="2176" x1="1280" />
            <wire x2="1648" y1="2176" y2="2416" x1="1648" />
            <wire x2="1664" y1="2416" y2="2416" x1="1648" />
        </branch>
        <branch name="XLXN_8(31:0)">
            <wire x2="1472" y1="2624" y2="2624" x1="1280" />
            <wire x2="1472" y1="2480" y2="2624" x1="1472" />
            <wire x2="1664" y1="2480" y2="2480" x1="1472" />
        </branch>
        <branch name="XLXN_9(31:0)">
            <wire x2="1552" y1="3008" y2="3008" x1="1264" />
            <wire x2="1552" y1="3008" y2="3104" x1="1552" />
            <wire x2="1840" y1="3104" y2="3104" x1="1552" />
        </branch>
        <branch name="XLXN_10(31:0)">
            <wire x2="1552" y1="3392" y2="3392" x1="1264" />
            <wire x2="1552" y1="3136" y2="3392" x1="1552" />
            <wire x2="1840" y1="3136" y2="3136" x1="1552" />
        </branch>
        <branch name="res2(31:0)">
            <wire x2="1552" y1="1840" y2="1840" x1="752" />
            <wire x2="4336" y1="1840" y2="1840" x1="1552" />
            <wire x2="752" y1="1840" y2="2368" x1="752" />
            <wire x2="752" y1="2368" y2="2816" x1="752" />
            <wire x2="752" y1="2816" y2="3200" x1="752" />
            <wire x2="752" y1="3200" y2="3584" x1="752" />
            <wire x2="832" y1="3584" y2="3584" x1="752" />
            <wire x2="832" y1="3200" y2="3200" x1="752" />
            <wire x2="848" y1="2816" y2="2816" x1="752" />
            <wire x2="848" y1="2368" y2="2368" x1="752" />
            <wire x2="4320" y1="880" y2="880" x1="4160" />
            <wire x2="4336" y1="880" y2="880" x1="4320" />
            <wire x2="5056" y1="880" y2="880" x1="4336" />
            <wire x2="4336" y1="880" y2="1568" x1="4336" />
            <wire x2="4336" y1="1568" y2="1840" x1="4336" />
            <wire x2="4336" y1="1568" y2="1568" x1="4176" />
        </branch>
        <instance x="5056" y="912" name="XLXI_11" orien="R0">
        </instance>
        <branch name="res(31:0)">
            <wire x2="5664" y1="688" y2="688" x1="5440" />
        </branch>
        <iomarker fontsize="28" x="5664" y="688" name="res(31:0)" orien="R0" />
        <branch name="Counter(3:0)">
            <wire x2="432" y1="432" y2="432" x1="384" />
            <wire x2="384" y1="432" y2="576" x1="384" />
            <wire x2="880" y1="576" y2="576" x1="384" />
            <wire x2="896" y1="576" y2="576" x1="880" />
            <wire x2="896" y1="576" y2="800" x1="896" />
            <wire x2="1024" y1="576" y2="576" x1="896" />
            <wire x2="896" y1="800" y2="800" x1="880" />
        </branch>
        <iomarker fontsize="28" x="1024" y="576" name="Counter(3:0)" orien="R0" />
        <branch name="a(7:0)">
            <wire x2="496" y1="1408" y2="1408" x1="320" />
        </branch>
        <branch name="b(7:0)">
            <wire x2="496" y1="1520" y2="1520" x1="320" />
        </branch>
        <branch name="c(7:0)">
            <wire x2="496" y1="1664" y2="1664" x1="320" />
        </branch>
        <branch name="d(7:0)">
            <wire x2="496" y1="1792" y2="1792" x1="320" />
        </branch>
        <iomarker fontsize="28" x="320" y="1408" name="a(7:0)" orien="R180" />
        <iomarker fontsize="28" x="320" y="1520" name="b(7:0)" orien="R180" />
        <iomarker fontsize="28" x="320" y="1664" name="c(7:0)" orien="R180" />
        <iomarker fontsize="28" x="320" y="1792" name="d(7:0)" orien="R180" />
        <branch name="clk">
            <wire x2="176" y1="864" y2="864" x1="80" />
            <wire x2="304" y1="864" y2="864" x1="176" />
            <wire x2="176" y1="864" y2="1120" x1="176" />
            <wire x2="1008" y1="1120" y2="1120" x1="176" />
            <wire x2="160" y1="1120" y2="1120" x1="80" />
            <wire x2="176" y1="1120" y2="1120" x1="160" />
            <wire x2="80" y1="1120" y2="2240" x1="80" />
            <wire x2="80" y1="2240" y2="2688" x1="80" />
            <wire x2="80" y1="2688" y2="3072" x1="80" />
            <wire x2="80" y1="3072" y2="3456" x1="80" />
            <wire x2="80" y1="3456" y2="3616" x1="80" />
            <wire x2="96" y1="3616" y2="3616" x1="80" />
            <wire x2="720" y1="3616" y2="3616" x1="96" />
            <wire x2="720" y1="3616" y2="3680" x1="720" />
            <wire x2="1680" y1="3680" y2="3680" x1="720" />
            <wire x2="832" y1="3456" y2="3456" x1="80" />
            <wire x2="832" y1="3072" y2="3072" x1="80" />
            <wire x2="848" y1="2688" y2="2688" x1="80" />
            <wire x2="848" y1="2240" y2="2240" x1="80" />
            <wire x2="432" y1="368" y2="368" x1="176" />
            <wire x2="176" y1="368" y2="864" x1="176" />
            <wire x2="1008" y1="704" y2="880" x1="1008" />
            <wire x2="1168" y1="880" y2="880" x1="1008" />
            <wire x2="1008" y1="880" y2="1120" x1="1008" />
            <wire x2="1888" y1="704" y2="704" x1="1008" />
            <wire x2="2272" y1="704" y2="704" x1="1888" />
            <wire x2="2976" y1="704" y2="704" x1="2272" />
            <wire x2="2976" y1="704" y2="880" x1="2976" />
            <wire x2="3008" y1="880" y2="880" x1="2976" />
            <wire x2="3600" y1="704" y2="704" x1="2976" />
            <wire x2="3856" y1="704" y2="704" x1="3600" />
            <wire x2="3856" y1="704" y2="816" x1="3856" />
            <wire x2="5056" y1="816" y2="816" x1="3856" />
            <wire x2="3600" y1="704" y2="1072" x1="3600" />
            <wire x2="3712" y1="1072" y2="1072" x1="3600" />
            <wire x2="1888" y1="704" y2="1072" x1="1888" />
            <wire x2="2016" y1="1072" y2="1072" x1="1888" />
            <wire x2="1664" y1="2576" y2="2576" x1="1616" />
            <wire x2="1616" y1="2576" y2="2816" x1="1616" />
            <wire x2="1680" y1="2816" y2="2816" x1="1616" />
            <wire x2="1680" y1="2816" y2="3168" x1="1680" />
            <wire x2="1680" y1="3168" y2="3680" x1="1680" />
            <wire x2="1840" y1="3168" y2="3168" x1="1680" />
        </branch>
        <iomarker fontsize="28" x="80" y="864" name="clk" orien="R180" />
        <branch name="XLXN_32(31:0)">
            <wire x2="1024" y1="1664" y2="1664" x1="880" />
            <wire x2="1024" y1="1008" y2="1664" x1="1024" />
            <wire x2="1168" y1="1008" y2="1008" x1="1024" />
        </branch>
        <branch name="XLXN_33(31:0)">
            <wire x2="2688" y1="1520" y2="1520" x1="880" />
            <wire x2="3008" y1="1008" y2="1008" x1="2688" />
            <wire x2="2688" y1="1008" y2="1504" x1="2688" />
            <wire x2="2688" y1="1504" y2="1520" x1="2688" />
        </branch>
        <branch name="XLXN_35(31:0)">
            <wire x2="2528" y1="1792" y2="1792" x1="880" />
            <wire x2="2528" y1="1072" y2="1792" x1="2528" />
            <wire x2="3008" y1="1072" y2="1072" x1="2528" />
        </branch>
        <branch name="XLXN_36(31:0)">
            <wire x2="1040" y1="1408" y2="1408" x1="880" />
            <wire x2="1040" y1="1072" y2="1408" x1="1040" />
            <wire x2="1168" y1="1072" y2="1072" x1="1040" />
        </branch>
        <branch name="XLXN_37(31:0)">
            <wire x2="1440" y1="640" y2="640" x1="1136" />
            <wire x2="1456" y1="640" y2="640" x1="1440" />
            <wire x2="1136" y1="640" y2="1136" x1="1136" />
            <wire x2="1168" y1="1136" y2="1136" x1="1136" />
            <wire x2="1456" y1="560" y2="640" x1="1456" />
            <wire x2="2768" y1="560" y2="560" x1="1456" />
            <wire x2="3120" y1="560" y2="560" x1="2768" />
            <wire x2="4608" y1="560" y2="560" x1="3120" />
            <wire x2="4608" y1="560" y2="576" x1="4608" />
            <wire x2="4640" y1="576" y2="576" x1="4608" />
            <wire x2="4640" y1="576" y2="2400" x1="4640" />
            <wire x2="4640" y1="2400" y2="2416" x1="4640" />
            <wire x2="2768" y1="560" y2="1136" x1="2768" />
            <wire x2="3008" y1="1136" y2="1136" x1="2768" />
            <wire x2="4640" y1="2416" y2="2416" x1="2240" />
        </branch>
        <branch name="XLXN_40(31:0)">
            <wire x2="1296" y1="672" y2="672" x1="1104" />
            <wire x2="1312" y1="672" y2="672" x1="1296" />
            <wire x2="1104" y1="672" y2="1200" x1="1104" />
            <wire x2="1168" y1="1200" y2="1200" x1="1104" />
            <wire x2="2560" y1="592" y2="592" x1="1312" />
            <wire x2="2864" y1="592" y2="592" x1="2560" />
            <wire x2="4512" y1="592" y2="592" x1="2864" />
            <wire x2="4512" y1="592" y2="608" x1="4512" />
            <wire x2="4544" y1="608" y2="608" x1="4512" />
            <wire x2="4544" y1="608" y2="3136" x1="4544" />
            <wire x2="2864" y1="592" y2="1200" x1="2864" />
            <wire x2="3008" y1="1200" y2="1200" x1="2864" />
            <wire x2="1312" y1="592" y2="672" x1="1312" />
            <wire x2="4544" y1="3136" y2="3136" x1="2128" />
        </branch>
        <branch name="CE">
            <wire x2="1840" y1="3200" y2="3200" x1="1824" />
            <wire x2="1824" y1="3200" y2="3264" x1="1824" />
            <wire x2="1824" y1="3264" y2="3728" x1="1824" />
            <wire x2="1888" y1="3728" y2="3728" x1="1824" />
            <wire x2="3376" y1="3728" y2="3728" x1="1888" />
            <wire x2="3376" y1="3728" y2="3760" x1="3376" />
            <wire x2="3376" y1="3760" y2="3776" x1="3376" />
            <wire x2="4464" y1="3776" y2="3776" x1="3376" />
            <wire x2="1840" y1="3264" y2="3264" x1="1824" />
            <wire x2="1984" y1="400" y2="400" x1="1888" />
            <wire x2="2656" y1="400" y2="400" x1="1984" />
            <wire x2="3632" y1="400" y2="400" x1="2656" />
            <wire x2="3968" y1="400" y2="400" x1="3632" />
            <wire x2="4448" y1="400" y2="400" x1="3968" />
            <wire x2="4464" y1="400" y2="400" x1="4448" />
            <wire x2="4464" y1="400" y2="3776" x1="4464" />
            <wire x2="3632" y1="400" y2="944" x1="3632" />
            <wire x2="3712" y1="944" y2="944" x1="3632" />
            <wire x2="1984" y1="400" y2="944" x1="1984" />
            <wire x2="2016" y1="944" y2="944" x1="1984" />
        </branch>
        <iomarker fontsize="28" x="1888" y="400" name="CE" orien="R180" />
        <branch name="ZERO_bit">
            <wire x2="2112" y1="368" y2="368" x1="1952" />
            <wire x2="2656" y1="368" y2="368" x1="2112" />
            <wire x2="3616" y1="368" y2="368" x1="2656" />
            <wire x2="3616" y1="368" y2="880" x1="3616" />
            <wire x2="3712" y1="880" y2="880" x1="3616" />
            <wire x2="2112" y1="368" y2="784" x1="2112" />
            <wire x2="1952" y1="784" y2="880" x1="1952" />
            <wire x2="2016" y1="880" y2="880" x1="1952" />
            <wire x2="2112" y1="784" y2="784" x1="1952" />
        </branch>
        <branch name="Control(19:0)">
            <wire x2="576" y1="3920" y2="3920" x1="480" />
            <wire x2="624" y1="3920" y2="3920" x1="576" />
            <wire x2="672" y1="3920" y2="3920" x1="624" />
            <wire x2="720" y1="3920" y2="3920" x1="672" />
            <wire x2="2176" y1="3920" y2="3920" x1="720" />
            <wire x2="3632" y1="3920" y2="3920" x1="2176" />
            <wire x2="4736" y1="3920" y2="3920" x1="3632" />
            <wire x2="1264" y1="368" y2="368" x1="816" />
            <wire x2="1712" y1="368" y2="368" x1="1264" />
            <wire x2="1712" y1="240" y2="368" x1="1712" />
            <wire x2="2016" y1="240" y2="240" x1="1712" />
            <wire x2="2080" y1="240" y2="240" x1="2016" />
            <wire x2="2928" y1="240" y2="240" x1="2080" />
            <wire x2="3008" y1="240" y2="240" x1="2928" />
            <wire x2="3648" y1="240" y2="240" x1="3008" />
            <wire x2="3712" y1="240" y2="240" x1="3648" />
            <wire x2="4560" y1="240" y2="240" x1="3712" />
            <wire x2="4736" y1="240" y2="240" x1="4560" />
            <wire x2="4736" y1="240" y2="688" x1="4736" />
            <wire x2="4736" y1="688" y2="752" x1="4736" />
            <wire x2="4736" y1="752" y2="3920" x1="4736" />
        </branch>
        <iomarker fontsize="28" x="480" y="3920" name="Control(19:0)" orien="R180" />
        <bustap x2="576" y1="3920" y2="3824" x1="576" />
        <bustap x2="624" y1="3920" y2="3824" x1="624" />
        <bustap x2="672" y1="3920" y2="3824" x1="672" />
        <bustap x2="720" y1="3920" y2="3824" x1="720" />
        <branch name="Control(3)">
            <wire x2="720" y1="3760" y2="3824" x1="720" />
            <wire x2="784" y1="3760" y2="3760" x1="720" />
            <wire x2="784" y1="3392" y2="3760" x1="784" />
            <wire x2="832" y1="3392" y2="3392" x1="784" />
        </branch>
        <branch name="Control(2)">
            <wire x2="832" y1="3008" y2="3008" x1="672" />
            <wire x2="672" y1="3008" y2="3824" x1="672" />
        </branch>
        <branch name="Control(1)">
            <wire x2="848" y1="2624" y2="2624" x1="624" />
            <wire x2="624" y1="2624" y2="3824" x1="624" />
        </branch>
        <branch name="Control(0)">
            <wire x2="848" y1="2176" y2="2176" x1="560" />
            <wire x2="560" y1="2176" y2="3824" x1="560" />
            <wire x2="576" y1="3824" y2="3824" x1="560" />
        </branch>
        <bustap x2="4832" y1="688" y2="688" x1="4736" />
        <bustap x2="4832" y1="752" y2="752" x1="4736" />
        <branch name="Control(5)">
            <wire x2="5056" y1="688" y2="688" x1="4832" />
        </branch>
        <branch name="Control(4)">
            <wire x2="5056" y1="752" y2="752" x1="4832" />
        </branch>
        <bustap x2="3712" y1="240" y2="336" x1="3712" />
        <bustap x2="3648" y1="240" y2="336" x1="3648" />
        <branch name="Control(9:6)">
            <wire x2="3712" y1="384" y2="384" x1="3648" />
            <wire x2="3648" y1="384" y2="1136" x1="3648" />
            <wire x2="3712" y1="1136" y2="1136" x1="3648" />
            <wire x2="3712" y1="336" y2="384" x1="3712" />
        </branch>
        <branch name="Control(10)">
            <wire x2="3648" y1="336" y2="352" x1="3648" />
            <wire x2="3696" y1="352" y2="352" x1="3648" />
            <wire x2="3696" y1="352" y2="1008" x1="3696" />
            <wire x2="3712" y1="1008" y2="1008" x1="3696" />
        </branch>
        <bustap x2="2928" y1="240" y2="336" x1="2928" />
        <branch name="Control(12:11)">
            <wire x2="2928" y1="336" y2="944" x1="2928" />
            <wire x2="3008" y1="944" y2="944" x1="2928" />
        </branch>
        <bustap x2="2080" y1="240" y2="336" x1="2080" />
        <bustap x2="2016" y1="240" y2="336" x1="2016" />
        <branch name="Control(16:13)">
            <wire x2="1968" y1="800" y2="1136" x1="1968" />
            <wire x2="2016" y1="1136" y2="1136" x1="1968" />
            <wire x2="2080" y1="800" y2="800" x1="1968" />
            <wire x2="2080" y1="336" y2="800" x1="2080" />
        </branch>
        <iomarker fontsize="28" x="1952" y="368" name="ZERO_bit" orien="R180" />
        <branch name="Control(17)">
            <wire x2="2016" y1="384" y2="384" x1="2000" />
            <wire x2="2000" y1="384" y2="1008" x1="2000" />
            <wire x2="2016" y1="1008" y2="1008" x1="2000" />
            <wire x2="2016" y1="336" y2="384" x1="2016" />
        </branch>
        <bustap x2="1264" y1="368" y2="464" x1="1264" />
        <branch name="Control(19:18)">
            <wire x2="1120" y1="784" y2="944" x1="1120" />
            <wire x2="1168" y1="944" y2="944" x1="1120" />
            <wire x2="1264" y1="784" y2="784" x1="1120" />
            <wire x2="1264" y1="464" y2="784" x1="1264" />
        </branch>
        <iomarker fontsize="28" x="2848" y="1648" name="res1(31:0)" orien="R0" />
        <iomarker fontsize="28" x="4176" y="1568" name="res2(31:0)" orien="R180" />
    </sheet>
</drawing>