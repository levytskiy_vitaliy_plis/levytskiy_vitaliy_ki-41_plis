-- Vhdl test bench created from schematic C:\PLIS\LVP_Lab4\lvp_sw.sch - Sat Dec 10 21:43:15 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY lvp_sw_lvp_sw_sch_tb IS
END lvp_sw_lvp_sw_sch_tb;
ARCHITECTURE behavioral OF lvp_sw_lvp_sw_sch_tb IS 

   COMPONENT lvp_sw
   PORT( c	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          CX	:	IN	STD_LOGIC; 
          CLR	:	IN	STD_LOGIC; 
          b	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          a	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          d	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          CE	:	IN	STD_LOGIC; 
          res	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0));
   END COMPONENT;

   SIGNAL c	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL CX	:	STD_LOGIC := '1';
   SIGNAL CLR	:	STD_LOGIC := '0';
   SIGNAL b	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL a	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL d	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL CE	:	STD_LOGIC := '1';
   SIGNAL res	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
	constant clk_c : time := 10 ns;
	SIGNAL buf: integer := 0;

BEGIN

   UUT: lvp_sw PORT MAP(
		c => c, 
		CX => CX, 
		CLR => CLR, 
		b => b, 
		a => a, 
		d => d, 
		CE => CE, 
		res => res
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
  BEGIN
      CX <= '0';
	wait for clk_c/2;
	buf <= buf + 1;
	a <= "00000010";
	b <= "00000011";
	c <= "00000011";
	d <= "00000010";
	CX <= '1';
	wait for clk_c/2;
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
