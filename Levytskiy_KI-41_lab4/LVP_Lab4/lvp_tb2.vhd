-- Vhdl test bench created from schematic C:\PLIS\LVP_Lab4\lvp_sw2.sch - Sun Dec 11 19:31:19 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY lvp_sw2_lvp_sw2_sch_tb IS
END lvp_sw2_lvp_sw2_sch_tb;
ARCHITECTURE behavioral OF lvp_sw2_lvp_sw2_sch_tb IS 

   COMPONENT lvp_sw2
   PORT( res1	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0); 
          res2	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0); 
          res	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0); 
          Counter	:	OUT	STD_LOGIC_VECTOR (3 DOWNTO 0); 
          a	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          b	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          c	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          d	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          clk	:	IN	STD_LOGIC; 
          CE	:	IN	STD_LOGIC; 
          ZERO_bit	:	IN	STD_LOGIC; 
          Control	:	OUT	STD_LOGIC_VECTOR (19 DOWNTO 0));
   END COMPONENT;

   SIGNAL res1	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
   SIGNAL res2	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
   SIGNAL res	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
   SIGNAL Counter	:	STD_LOGIC_VECTOR (3 DOWNTO 0);
   SIGNAL a	:	STD_LOGIC_VECTOR (7 DOWNTO 0) :=x"02";
   SIGNAL b	:	STD_LOGIC_VECTOR (7 DOWNTO 0) :=x"03";
   SIGNAL c	:	STD_LOGIC_VECTOR (7 DOWNTO 0) :=x"03";
   SIGNAL d	:	STD_LOGIC_VECTOR (7 DOWNTO 0) :=x"02";
   SIGNAL clk	:	STD_LOGIC;
   SIGNAL CE	:	STD_LOGIC :='1';
   SIGNAL ZERO_bit	:	STD_LOGIC :='0';
   SIGNAL Control	:	STD_LOGIC_VECTOR (19 DOWNTO 0);
	constant clk_c : time := 20 ns;

BEGIN

   UUT: lvp_sw2 PORT MAP(
		res1 => res1, 
		res2 => res2, 
		res => res, 
		Counter => Counter, 
		a => a, 
		b => b, 
		c => c, 
		d => d, 
		clk => clk, 
		CE => CE, 
		ZERO_bit => ZERO_bit, 
		Control => Control
   );
	CLC_process :process

	begin
	CLK <= '1';
	wait for clk_c/2;
		
	CLK <= '0';
	wait for clk_c/2;
	end process;

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
