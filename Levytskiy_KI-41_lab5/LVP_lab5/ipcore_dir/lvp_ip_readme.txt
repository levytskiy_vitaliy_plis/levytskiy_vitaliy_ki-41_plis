The following files were generated for 'lvp_ip' in directory
C:\PLIS\TYR_Lab5 (1)\LVP_lab5\ipcore_dir\

Opens the IP Customization GUI:
   Allows the user to customize or recustomize the IP instance.

   * lvp_ip.mif

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * lvp_ip.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * lvp_ip.ngc
   * lvp_ip.v
   * lvp_ip.veo
   * lvp_ipCOEFF_auto0_0.mif
   * lvp_ipCOEFF_auto0_1.mif
   * lvp_ipCOEFF_auto0_2.mif
   * lvp_ipCOEFF_auto0_3.mif
   * lvp_ipCOEFF_auto0_4.mif
   * lvp_ipCOEFF_auto0_5.mif
   * lvp_ipCOEFF_auto0_6.mif
   * lvp_ipfilt_decode_rom.mif

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * lvp_ip.veo

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * lvp_ip.asy
   * lvp_ip.mif

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * lvp_ip.sym

Generate ISE metadata:
   Create a metadata file for use when including this core in ISE designs

   * lvp_ip_xmdf.tcl

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * lvp_ip.gise
   * lvp_ip.xise

Deliver Readme:
   Readme file for the IP.

   * lvp_ip_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * lvp_ip_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

