----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:27:05 12/15/2016 
-- Design Name: 
-- Module Name:    lvp_mod2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lvp_mod2 is
    Port ( LVP_A : in  STD_LOGIC_VECTOR (7 downto 0);
           LVP_out2 : out  STD_LOGIC_VECTOR (15 downto 0));
end lvp_mod2;

architecture Behavioral of lvp_mod2 is
signal lvp_prod,lvp_p7,lvp_p6,lvp_p5,lvp_p4,lvp_p3,lvp_p2,lvp_p1,lvp_p0:unsigned (15 downto 0);
begin
lvp_p3  <="00000" & unsigned (lvp_A) & "000";
lvp_p5  <="000" & unsigned (lvp_A) & "00000";
lvp_p6  <="00" & unsigned (lvp_A) & "000000";
lvp_prod <= lvp_p3 + lvp_p5 + lvp_p6;
lvp_out2 <= std_logic_vector (lvp_prod);

end Behavioral;

