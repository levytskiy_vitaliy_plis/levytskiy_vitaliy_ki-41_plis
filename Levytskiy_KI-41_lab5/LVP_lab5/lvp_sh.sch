<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="LVP_A(7:0)" />
        <signal name="XLXN_3(7:0)" />
        <signal name="XLXN_6(7:0)" />
        <signal name="XLXN_7(7:0)" />
        <signal name="XLXN_10(7:0)" />
        <signal name="XLXN_11(7:0)" />
        <signal name="XLXN_13(7:0)" />
        <signal name="CE" />
        <signal name="CLK" />
        <signal name="CLR" />
        <signal name="XLXN_17(15:0)" />
        <signal name="XLXN_18(15:0)" />
        <signal name="XLXN_19(15:0)" />
        <signal name="XLXN_20(15:0)" />
        <signal name="XLXN_21" />
        <signal name="XLXN_22" />
        <signal name="XLXN_23(15:0)" />
        <signal name="XLXN_24(15:0)" />
        <signal name="XLXN_25(15:0)" />
        <signal name="XLXN_26(15:0)" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30(15:0)" />
        <signal name="XLXN_31(15:0)" />
        <signal name="XLXN_32" />
        <signal name="XLXN_33(15:0)" />
        <signal name="XLXN_34(15:0)" />
        <signal name="XLXN_35" />
        <signal name="XLXN_36(15:0)" />
        <signal name="res(15:0)" />
        <signal name="XLXN_43" />
        <signal name="XLXN_44" />
        <signal name="XLXN_45" />
        <signal name="XLXN_46" />
        <port polarity="Input" name="LVP_A(7:0)" />
        <port polarity="Input" name="CE" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="CLR" />
        <port polarity="Output" name="res(15:0)" />
        <blockdef name="lvp_mod1">
            <timestamp>2016-12-15T9:26:39</timestamp>
            <rect width="304" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-44" height="24" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="lvp_mod2">
            <timestamp>2016-12-15T9:28:52</timestamp>
            <rect width="304" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-44" height="24" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="fd8ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="fd16ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <block symbolname="lvp_mod1" name="XLXI_1">
            <blockpin signalname="LVP_A(7:0)" name="LVP_A(7:0)" />
            <blockpin signalname="XLXN_17(15:0)" name="LVP_out1(15:0)" />
        </block>
        <block symbolname="lvp_mod1" name="XLXI_2">
            <blockpin signalname="XLXN_3(7:0)" name="LVP_A(7:0)" />
            <blockpin signalname="XLXN_18(15:0)" name="LVP_out1(15:0)" />
        </block>
        <block symbolname="lvp_mod1" name="XLXI_3">
            <blockpin signalname="XLXN_6(7:0)" name="LVP_A(7:0)" />
            <blockpin signalname="XLXN_20(15:0)" name="LVP_out1(15:0)" />
        </block>
        <block symbolname="lvp_mod2" name="XLXI_4">
            <blockpin signalname="XLXN_7(7:0)" name="LVP_A(7:0)" />
            <blockpin signalname="XLXN_24(15:0)" name="LVP_out2(15:0)" />
        </block>
        <block symbolname="lvp_mod2" name="XLXI_5">
            <blockpin signalname="XLXN_10(7:0)" name="LVP_A(7:0)" />
            <blockpin signalname="XLXN_26(15:0)" name="LVP_out2(15:0)" />
        </block>
        <block symbolname="lvp_mod2" name="XLXI_6">
            <blockpin signalname="XLXN_11(7:0)" name="LVP_A(7:0)" />
            <blockpin signalname="XLXN_31(15:0)" name="LVP_out2(15:0)" />
        </block>
        <block symbolname="lvp_mod2" name="XLXI_7">
            <blockpin signalname="XLXN_13(7:0)" name="LVP_A(7:0)" />
            <blockpin signalname="XLXN_34(15:0)" name="LVP_out2(15:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_8">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="LVP_A(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_3(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_9">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_3(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_6(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_10">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_6(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_7(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_11">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_7(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_10(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_12">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_10(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_11(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_13">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_11(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_13(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="add16" name="XLXI_14">
            <blockpin signalname="XLXN_17(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_18(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_46" name="CI" />
            <blockpin signalname="XLXN_21" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_19(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_15">
            <blockpin signalname="XLXN_20(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_19(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_21" name="CI" />
            <blockpin signalname="XLXN_22" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_23(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_16">
            <blockpin signalname="XLXN_24(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_23(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_22" name="CI" />
            <blockpin signalname="XLXN_29" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_25(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_17">
            <blockpin signalname="XLXN_26(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_25(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_29" name="CI" />
            <blockpin signalname="XLXN_32" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_30(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_18">
            <blockpin signalname="XLXN_31(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_30(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_32" name="CI" />
            <blockpin signalname="XLXN_35" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_33(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_19">
            <blockpin signalname="XLXN_34(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_33(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_35" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_36(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_20">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_36(15:0)" name="D(15:0)" />
            <blockpin signalname="res(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_21">
            <blockpin signalname="XLXN_46" name="G" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="960" y="1504" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1776" y="1488" name="XLXI_2" orien="R0">
        </instance>
        <instance x="2560" y="1472" name="XLXI_3" orien="R0">
        </instance>
        <instance x="3440" y="1456" name="XLXI_4" orien="R0">
        </instance>
        <instance x="4256" y="1440" name="XLXI_5" orien="R0">
        </instance>
        <instance x="6000" y="1424" name="XLXI_7" orien="R0">
        </instance>
        <instance x="5152" y="1456" name="XLXI_6" orien="R0">
        </instance>
        <instance x="864" y="768" name="XLXI_8" orien="R0" />
        <instance x="1872" y="784" name="XLXI_9" orien="R0" />
        <instance x="2624" y="800" name="XLXI_10" orien="R0" />
        <instance x="3520" y="800" name="XLXI_11" orien="R0" />
        <instance x="4352" y="816" name="XLXI_12" orien="R0" />
        <instance x="5120" y="816" name="XLXI_13" orien="R0" />
        <branch name="LVP_A(7:0)">
            <wire x2="704" y1="512" y2="512" x1="528" />
            <wire x2="864" y1="512" y2="512" x1="704" />
            <wire x2="704" y1="512" y2="1472" x1="704" />
            <wire x2="960" y1="1472" y2="1472" x1="704" />
        </branch>
        <branch name="XLXN_3(7:0)">
            <wire x2="1552" y1="512" y2="512" x1="1248" />
            <wire x2="1552" y1="512" y2="528" x1="1552" />
            <wire x2="1584" y1="528" y2="528" x1="1552" />
            <wire x2="1872" y1="528" y2="528" x1="1584" />
            <wire x2="1584" y1="528" y2="1456" x1="1584" />
            <wire x2="1776" y1="1456" y2="1456" x1="1584" />
        </branch>
        <branch name="XLXN_6(7:0)">
            <wire x2="2432" y1="528" y2="528" x1="2256" />
            <wire x2="2432" y1="528" y2="544" x1="2432" />
            <wire x2="2624" y1="544" y2="544" x1="2432" />
            <wire x2="2432" y1="544" y2="1440" x1="2432" />
            <wire x2="2560" y1="1440" y2="1440" x1="2432" />
        </branch>
        <branch name="XLXN_7(7:0)">
            <wire x2="3280" y1="544" y2="544" x1="3008" />
            <wire x2="3520" y1="544" y2="544" x1="3280" />
            <wire x2="3280" y1="544" y2="1424" x1="3280" />
            <wire x2="3440" y1="1424" y2="1424" x1="3280" />
        </branch>
        <branch name="XLXN_10(7:0)">
            <wire x2="4128" y1="544" y2="544" x1="3904" />
            <wire x2="4128" y1="544" y2="560" x1="4128" />
            <wire x2="4352" y1="560" y2="560" x1="4128" />
            <wire x2="4128" y1="560" y2="1408" x1="4128" />
            <wire x2="4256" y1="1408" y2="1408" x1="4128" />
        </branch>
        <branch name="XLXN_11(7:0)">
            <wire x2="4912" y1="560" y2="560" x1="4736" />
            <wire x2="5120" y1="560" y2="560" x1="4912" />
            <wire x2="4912" y1="560" y2="1424" x1="4912" />
            <wire x2="5152" y1="1424" y2="1424" x1="4912" />
        </branch>
        <branch name="XLXN_13(7:0)">
            <wire x2="5744" y1="560" y2="560" x1="5504" />
            <wire x2="5744" y1="560" y2="1392" x1="5744" />
            <wire x2="6000" y1="1392" y2="1392" x1="5744" />
        </branch>
        <branch name="CE">
            <wire x2="640" y1="576" y2="576" x1="528" />
            <wire x2="864" y1="576" y2="576" x1="640" />
            <wire x2="640" y1="576" y2="896" x1="640" />
            <wire x2="640" y1="896" y2="2480" x1="640" />
            <wire x2="752" y1="2480" y2="2480" x1="640" />
            <wire x2="2032" y1="2480" y2="2480" x1="752" />
            <wire x2="3728" y1="2480" y2="2480" x1="2032" />
            <wire x2="3728" y1="2480" y2="2496" x1="3728" />
            <wire x2="3728" y1="2496" y2="2512" x1="3728" />
            <wire x2="6336" y1="2512" y2="2512" x1="3728" />
            <wire x2="1264" y1="896" y2="896" x1="640" />
            <wire x2="2320" y1="896" y2="896" x1="1264" />
            <wire x2="3072" y1="896" y2="896" x1="2320" />
            <wire x2="3968" y1="896" y2="896" x1="3072" />
            <wire x2="4816" y1="896" y2="896" x1="3968" />
            <wire x2="1264" y1="592" y2="896" x1="1264" />
            <wire x2="1872" y1="592" y2="592" x1="1264" />
            <wire x2="2320" y1="608" y2="896" x1="2320" />
            <wire x2="2624" y1="608" y2="608" x1="2320" />
            <wire x2="3072" y1="608" y2="896" x1="3072" />
            <wire x2="3520" y1="608" y2="608" x1="3072" />
            <wire x2="3968" y1="624" y2="896" x1="3968" />
            <wire x2="4352" y1="624" y2="624" x1="3968" />
            <wire x2="4816" y1="624" y2="896" x1="4816" />
            <wire x2="5120" y1="624" y2="624" x1="4816" />
        </branch>
        <branch name="CLK">
            <wire x2="672" y1="640" y2="640" x1="544" />
            <wire x2="864" y1="640" y2="640" x1="672" />
            <wire x2="672" y1="640" y2="976" x1="672" />
            <wire x2="672" y1="976" y2="2624" x1="672" />
            <wire x2="704" y1="2624" y2="2624" x1="672" />
            <wire x2="2272" y1="2624" y2="2624" x1="704" />
            <wire x2="3680" y1="2624" y2="2624" x1="2272" />
            <wire x2="1280" y1="976" y2="976" x1="672" />
            <wire x2="2304" y1="976" y2="976" x1="1280" />
            <wire x2="3056" y1="976" y2="976" x1="2304" />
            <wire x2="3952" y1="976" y2="976" x1="3056" />
            <wire x2="4800" y1="976" y2="976" x1="3952" />
            <wire x2="1280" y1="656" y2="976" x1="1280" />
            <wire x2="1872" y1="656" y2="656" x1="1280" />
            <wire x2="2304" y1="672" y2="976" x1="2304" />
            <wire x2="2624" y1="672" y2="672" x1="2304" />
            <wire x2="3056" y1="672" y2="976" x1="3056" />
            <wire x2="3520" y1="672" y2="672" x1="3056" />
            <wire x2="6336" y1="2576" y2="2576" x1="3680" />
            <wire x2="3680" y1="2576" y2="2608" x1="3680" />
            <wire x2="3680" y1="2608" y2="2624" x1="3680" />
            <wire x2="3952" y1="688" y2="976" x1="3952" />
            <wire x2="4352" y1="688" y2="688" x1="3952" />
            <wire x2="4800" y1="688" y2="976" x1="4800" />
            <wire x2="5120" y1="688" y2="688" x1="4800" />
        </branch>
        <branch name="CLR">
            <wire x2="864" y1="752" y2="752" x1="544" />
            <wire x2="912" y1="752" y2="752" x1="864" />
            <wire x2="1872" y1="752" y2="752" x1="912" />
            <wire x2="912" y1="752" y2="832" x1="912" />
            <wire x2="912" y1="832" y2="2704" x1="912" />
            <wire x2="2800" y1="2704" y2="2704" x1="912" />
            <wire x2="2624" y1="832" y2="832" x1="912" />
            <wire x2="3520" y1="832" y2="832" x1="2624" />
            <wire x2="4352" y1="832" y2="832" x1="3520" />
            <wire x2="5120" y1="832" y2="832" x1="4352" />
            <wire x2="864" y1="736" y2="752" x1="864" />
            <wire x2="2624" y1="768" y2="832" x1="2624" />
            <wire x2="4480" y1="2688" y2="2688" x1="2800" />
            <wire x2="2800" y1="2688" y2="2704" x1="2800" />
            <wire x2="3520" y1="768" y2="832" x1="3520" />
            <wire x2="4352" y1="784" y2="832" x1="4352" />
            <wire x2="6336" y1="2672" y2="2672" x1="4480" />
            <wire x2="4480" y1="2672" y2="2688" x1="4480" />
            <wire x2="5120" y1="784" y2="832" x1="5120" />
        </branch>
        <iomarker fontsize="28" x="528" y="512" name="LVP_A(7:0)" orien="R180" />
        <iomarker fontsize="28" x="528" y="576" name="CE" orien="R180" />
        <iomarker fontsize="28" x="544" y="640" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="544" y="752" name="CLR" orien="R180" />
        <instance x="1328" y="2272" name="XLXI_14" orien="R0" />
        <instance x="2304" y="2256" name="XLXI_15" orien="R0" />
        <instance x="3216" y="2256" name="XLXI_16" orien="R0" />
        <branch name="XLXN_17(15:0)">
            <wire x2="1264" y1="1760" y2="1952" x1="1264" />
            <wire x2="1328" y1="1952" y2="1952" x1="1264" />
            <wire x2="1472" y1="1760" y2="1760" x1="1264" />
            <wire x2="1472" y1="1472" y2="1472" x1="1392" />
            <wire x2="1472" y1="1472" y2="1760" x1="1472" />
        </branch>
        <branch name="XLXN_18(15:0)">
            <wire x2="1232" y1="1680" y2="2080" x1="1232" />
            <wire x2="1328" y1="2080" y2="2080" x1="1232" />
            <wire x2="2288" y1="1680" y2="1680" x1="1232" />
            <wire x2="2288" y1="1456" y2="1456" x1="2208" />
            <wire x2="2288" y1="1456" y2="1680" x1="2288" />
        </branch>
        <branch name="XLXN_19(15:0)">
            <wire x2="2032" y1="2016" y2="2016" x1="1776" />
            <wire x2="2032" y1="2016" y2="2064" x1="2032" />
            <wire x2="2304" y1="2064" y2="2064" x1="2032" />
        </branch>
        <branch name="XLXN_20(15:0)">
            <wire x2="3072" y1="1728" y2="1728" x1="2224" />
            <wire x2="2224" y1="1728" y2="1936" x1="2224" />
            <wire x2="2304" y1="1936" y2="1936" x1="2224" />
            <wire x2="3072" y1="1440" y2="1440" x1="2992" />
            <wire x2="3072" y1="1440" y2="1728" x1="3072" />
        </branch>
        <branch name="XLXN_21">
            <wire x2="2016" y1="2208" y2="2208" x1="1776" />
            <wire x2="2016" y1="1808" y2="2208" x1="2016" />
            <wire x2="2304" y1="1808" y2="1808" x1="2016" />
        </branch>
        <branch name="XLXN_22">
            <wire x2="2976" y1="2192" y2="2192" x1="2752" />
            <wire x2="2976" y1="1808" y2="2192" x1="2976" />
            <wire x2="3216" y1="1808" y2="1808" x1="2976" />
        </branch>
        <branch name="XLXN_23(15:0)">
            <wire x2="2960" y1="2000" y2="2000" x1="2752" />
            <wire x2="2960" y1="2000" y2="2064" x1="2960" />
            <wire x2="3216" y1="2064" y2="2064" x1="2960" />
        </branch>
        <branch name="XLXN_24(15:0)">
            <wire x2="3952" y1="1728" y2="1728" x1="3136" />
            <wire x2="3136" y1="1728" y2="1936" x1="3136" />
            <wire x2="3216" y1="1936" y2="1936" x1="3136" />
            <wire x2="3952" y1="1424" y2="1424" x1="3872" />
            <wire x2="3952" y1="1424" y2="1728" x1="3952" />
        </branch>
        <instance x="4224" y="2272" name="XLXI_17" orien="R0" />
        <branch name="XLXN_25(15:0)">
            <wire x2="3936" y1="2000" y2="2000" x1="3664" />
            <wire x2="3936" y1="2000" y2="2080" x1="3936" />
            <wire x2="4224" y1="2080" y2="2080" x1="3936" />
        </branch>
        <branch name="XLXN_26(15:0)">
            <wire x2="4144" y1="1760" y2="1952" x1="4144" />
            <wire x2="4224" y1="1952" y2="1952" x1="4144" />
            <wire x2="4736" y1="1760" y2="1760" x1="4144" />
            <wire x2="4736" y1="1408" y2="1408" x1="4688" />
            <wire x2="4736" y1="1408" y2="1760" x1="4736" />
        </branch>
        <branch name="XLXN_29">
            <wire x2="3920" y1="2192" y2="2192" x1="3664" />
            <wire x2="3920" y1="1824" y2="2192" x1="3920" />
            <wire x2="4224" y1="1824" y2="1824" x1="3920" />
        </branch>
        <instance x="5264" y="2320" name="XLXI_18" orien="R0" />
        <branch name="XLXN_30(15:0)">
            <wire x2="4960" y1="2016" y2="2016" x1="4672" />
            <wire x2="4960" y1="2016" y2="2128" x1="4960" />
            <wire x2="5264" y1="2128" y2="2128" x1="4960" />
        </branch>
        <branch name="XLXN_31(15:0)">
            <wire x2="5664" y1="1808" y2="1808" x1="5200" />
            <wire x2="5200" y1="1808" y2="2000" x1="5200" />
            <wire x2="5264" y1="2000" y2="2000" x1="5200" />
            <wire x2="5664" y1="1424" y2="1424" x1="5584" />
            <wire x2="5664" y1="1424" y2="1808" x1="5664" />
        </branch>
        <branch name="XLXN_32">
            <wire x2="4944" y1="2208" y2="2208" x1="4672" />
            <wire x2="4944" y1="1872" y2="2208" x1="4944" />
            <wire x2="5264" y1="1872" y2="1872" x1="4944" />
        </branch>
        <instance x="6096" y="2336" name="XLXI_19" orien="R0" />
        <branch name="XLXN_33(15:0)">
            <wire x2="5904" y1="2064" y2="2064" x1="5712" />
            <wire x2="5904" y1="2064" y2="2144" x1="5904" />
            <wire x2="6096" y1="2144" y2="2144" x1="5904" />
        </branch>
        <branch name="XLXN_34(15:0)">
            <wire x2="6032" y1="1808" y2="2016" x1="6032" />
            <wire x2="6096" y1="2016" y2="2016" x1="6032" />
            <wire x2="6512" y1="1808" y2="1808" x1="6032" />
            <wire x2="6512" y1="1392" y2="1392" x1="6432" />
            <wire x2="6512" y1="1392" y2="1808" x1="6512" />
        </branch>
        <branch name="XLXN_35">
            <wire x2="5888" y1="2256" y2="2256" x1="5712" />
            <wire x2="5888" y1="1888" y2="2256" x1="5888" />
            <wire x2="6096" y1="1888" y2="1888" x1="5888" />
        </branch>
        <instance x="6336" y="2704" name="XLXI_20" orien="R0" />
        <branch name="XLXN_36(15:0)">
            <wire x2="6272" y1="2304" y2="2448" x1="6272" />
            <wire x2="6336" y1="2448" y2="2448" x1="6272" />
            <wire x2="6624" y1="2304" y2="2304" x1="6272" />
            <wire x2="6624" y1="2080" y2="2080" x1="6544" />
            <wire x2="6624" y1="2080" y2="2304" x1="6624" />
        </branch>
        <branch name="res(15:0)">
            <wire x2="6848" y1="2448" y2="2448" x1="6720" />
        </branch>
        <iomarker fontsize="28" x="6848" y="2448" name="res(15:0)" orien="R0" />
        <instance x="1056" y="2256" name="XLXI_21" orien="R0" />
        <branch name="XLXN_46">
            <wire x2="1328" y1="1824" y2="1824" x1="1104" />
            <wire x2="1104" y1="1824" y2="2064" x1="1104" />
            <wire x2="1120" y1="2064" y2="2064" x1="1104" />
            <wire x2="1120" y1="2064" y2="2128" x1="1120" />
        </branch>
    </sheet>
</drawing>