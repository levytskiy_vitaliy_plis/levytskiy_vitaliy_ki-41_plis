<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="LVP_A(7:0)" />
        <signal name="XLXN_2(7:0)" />
        <signal name="CE" />
        <signal name="XLXN_4" />
        <signal name="CLR" />
        <signal name="CLK" />
        <signal name="rfd" />
        <signal name="rdy" />
        <signal name="res_ip(17:0)" />
        <signal name="res(15:0)" />
        <port polarity="Input" name="LVP_A(7:0)" />
        <port polarity="Input" name="CE" />
        <port polarity="Input" name="CLR" />
        <port polarity="Input" name="CLK" />
        <port polarity="Output" name="rfd" />
        <port polarity="Output" name="rdy" />
        <port polarity="Output" name="res_ip(17:0)" />
        <port polarity="Output" name="res(15:0)" />
        <blockdef name="lvp_sh">
            <timestamp>2016-12-15T12:7:25</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="lvp_ip">
            <timestamp>2016-12-15T12:4:49</timestamp>
            <rect width="512" x="32" y="32" height="2016" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="1008" y2="1008" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
            <line x2="544" y1="1840" y2="1840" x1="576" />
            <line x2="544" y1="1872" y2="1872" x1="576" />
        </blockdef>
        <block symbolname="lvp_sh" name="XLXI_1">
            <blockpin signalname="LVP_A(7:0)" name="LVP_A(7:0)" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="res(15:0)" name="res(15:0)" />
        </block>
        <block symbolname="lvp_ip" name="XLXI_2">
            <blockpin signalname="LVP_A(7:0)" name="din(7:0)" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="res_ip(17:0)" name="dout(17:0)" />
            <blockpin signalname="rfd" name="rfd" />
            <blockpin signalname="rdy" name="rdy" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="736" y="1632" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1584" y="368" name="XLXI_2" orien="R0">
        </instance>
        <branch name="LVP_A(7:0)">
            <wire x2="624" y1="448" y2="448" x1="352" />
            <wire x2="1584" y1="448" y2="448" x1="624" />
            <wire x2="624" y1="448" y2="1408" x1="624" />
            <wire x2="736" y1="1408" y2="1408" x1="624" />
        </branch>
        <branch name="CE">
            <wire x2="736" y1="1472" y2="1472" x1="384" />
            <wire x2="384" y1="1472" y2="1488" x1="384" />
        </branch>
        <branch name="CLR">
            <wire x2="736" y1="1600" y2="1600" x1="496" />
            <wire x2="496" y1="1600" y2="1616" x1="496" />
        </branch>
        <branch name="CLK">
            <wire x2="512" y1="1136" y2="1136" x1="416" />
            <wire x2="1200" y1="1136" y2="1136" x1="512" />
            <wire x2="1200" y1="1136" y2="1376" x1="1200" />
            <wire x2="1584" y1="1376" y2="1376" x1="1200" />
            <wire x2="512" y1="1136" y2="1520" x1="512" />
            <wire x2="448" y1="1520" y2="1536" x1="448" />
            <wire x2="736" y1="1536" y2="1536" x1="448" />
            <wire x2="512" y1="1520" y2="1520" x1="448" />
        </branch>
        <branch name="rfd">
            <wire x2="2304" y1="2208" y2="2208" x1="2160" />
        </branch>
        <branch name="rdy">
            <wire x2="2384" y1="2240" y2="2240" x1="2160" />
        </branch>
        <branch name="res_ip(17:0)">
            <wire x2="2272" y1="448" y2="448" x1="2160" />
            <wire x2="2272" y1="432" y2="448" x1="2272" />
        </branch>
        <branch name="res(15:0)">
            <wire x2="1232" y1="1408" y2="1408" x1="1120" />
            <wire x2="1232" y1="1408" y2="1600" x1="1232" />
        </branch>
        <iomarker fontsize="28" x="496" y="1616" name="CLR" orien="R90" />
        <iomarker fontsize="28" x="384" y="1488" name="CE" orien="R90" />
        <iomarker fontsize="28" x="416" y="1136" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="352" y="448" name="LVP_A(7:0)" orien="R180" />
        <iomarker fontsize="28" x="2272" y="432" name="res_ip(17:0)" orien="R270" />
        <iomarker fontsize="28" x="2304" y="2208" name="rfd" orien="R0" />
        <iomarker fontsize="28" x="2384" y="2240" name="rdy" orien="R0" />
        <iomarker fontsize="28" x="1232" y="1600" name="res(15:0)" orien="R90" />
    </sheet>
</drawing>